<?php

use GuzzleHttp\Client;
use Goutte\Client as Crawl;
use Phalcon\Mvc\Url;

class EventController extends ControllerBase
{
  public function IndexAction()
  {
    $events = $this->getRecent();
    //return $this->response->setJsonContent($events);

    $this->view->setVar('events',$events);

  }

  public function getRecent()
  {
    $url = "https://www.dotabuff.com/procircuit/events";
    $client = new Crawl();
    $crawler = $client->request('GET', $url);

    $event = $crawler->filter('a.pc-event')->each(function ($tr, $i){
      $event_image = $tr->filter('div.image img')->attr('src');
      $event_title = $tr->filter('div.description .title')->text();
      $event_start = $tr->filter('div.line div.datetime')->text();
      $event_type = $tr->filter('div.designation')->text();
      $team = $tr->filter('div.invites-placements div.placement')->each(function ($tr2, $i2){

        $team_detail = (object) [
          'place' => $tr2->text(),
          'image' => $tr2->filter('img.img-team')->attr('src'),
        ];

        return $team_detail;
      //return $tr2->html();
      });
      $event = (object) [
        'event_image' => $event_image,
        'event_title' => $event_title,
        'event_start' => $event_start,
        'event_type' => $event_type,
        'team' => $team,
      ];

      return $event;



    });
    return $event;
    //return $this->response->setJsonContent($event);
    // $recent = $crawler->filter('div.content-inner .col-8 .home-blog-post')->each(function ($tr, $i) {
    //   $news_name = $tr->filter('header a')->text();
    //   $news_time = ($tr->filter('header time')->count()) ? $tr->filter('header time')->text() : 'Today';
    //   // $news_time = $tr->filter('header time')->text();
    //   $news_image = $tr->filter('article div.image img')->attr('src');
    //   $news_summary = $tr->filter('article div.summary p')->text();
    //   $news = (object) [
    //     'news_name' => $news_name,
    //     'news_image' => $news_image,
    //     'news_summary' => $news_summary,
    //     'news_time' => $news_time,
    //   ];
    //   return $news;
    // });
  }
}
