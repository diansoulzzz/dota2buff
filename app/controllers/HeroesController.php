<?php

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Phalcon\Mvc\Url;
use Goutte\Client as Crawl;

class HeroesController extends ControllerBase
{
    public function getHeroesData()
    {
       $url = "https://api.opendota.com/api/heroStats";
       $client = new Client();
       $response = $client->request('GET', $url);
       $data = json_decode($response->getBody());
       // return $this->response->setJsonContent(json_decode($response->getBody()));
       // $this->view->setVar('heroes',$data);
       return $data;
    }
    public function getHeroSkill($link_url){
      $url = "https://www.dotabuff.com/heroes/".$link_url."/abilities";
      $client = new Crawl();
      $crawler = $client->request('GET', $url);
      // return $url;
      $abilities = $crawler->filter('div .content-inner .col-8 section')->each(function ($crawl, $i) {
        $ability = (object) [
          'ability_name' => preg_replace("(<([a-z]+)>.*?</\\1>)is","",$crawl->filter('header')->html()),
          'ability_image' => 'https://www.dotabuff.com'.$crawl->filter('img.image-bigavatar.image-skill')->attr('src'),
          'ability_desc' => $crawl->filter('.description p')->text(),
        ];
        $ability->ability_detail = $crawl->filter('.effects p')->each(function ($effect, $j){
          $effect_stats = explode(':',$effect->text());
          return (object) [
            'type' => $effect->filter('span')->attr('class'),
            'name' => $effect_stats[0],
            'value' => $effect_stats[1],
          ];
        });
        $ability->ability_stats = $crawl->filter('.stats .stat.effect')->each(function ($stats, $j){
          return (object) [
            'name' => $stats->filter('span.label')->text(),
            'value' => $stats->filter('span.values')->text(),
          ];
        });
        $ability->ability_cooldown = $crawl->filter('.cooldown_and_cost .cooldown .value .number')->each(function ($cooldown, $j){
          return $cooldown->text();
        });
        $ability->ability_manacost = $crawl->filter('.cooldown_and_cost .manacost .value .number')->each(function ($manacost, $j){
          return $manacost->text();
        });
        return $ability;
      });
      $talent = $crawler->filter('div .content-inner .col-4 .hero-talents tbody tr')->each(function ($crawl, $i) {
        return (object) [
          'level' => $crawl->filter('.talent-level')->text(),
          'left' => $crawl->filter('td')->first()->text(),
          'right' => $crawl->filter('td')->last()->text(),
        ];
      });
      // $attributes = $crawler->filter('div .content-inner .col-4 .hero-talents tbody tr')->each(function ($crawl, $i) {
      //
      // });
      $results = (object) [
        'abilities' => $abilities,
        'talent' => $talent,
      ];
      return $results;
    }
    public function getHeroesDetail($heroesid){
      $client = new Client(['base_uri' => 'https://api.opendota.com/api/heroes/']);
      $promises = [
          'matches'  => $client->getAsync($heroesid."/matches"),
          'heroStats'  => $client->getAsync("/api/heroStats/"),
      ];
      $results = Promise\unwrap($promises);
      $results = Promise\settle($promises)->wait();

      $profile = (object) [
          'matches' => json_decode($results['matches']['value']->getBody()),
      ];
      $heroes = json_decode($results['heroStats']['value']->getBody());
      foreach ($heroes as $key => $value) {
        if ($value->id==$heroesid)
        {
            $profile->info = $value;
        }
      }
      $skill = $this->getHeroSkill(str_replace(' ','-',strtolower($profile->info->localized_name)));
      $profile->skill = $skill;
      return $profile;
    }
    public function indexAction()
    {
      $heroes = $this->getHeroesData();
      $this->view->setVar('heroes',$this->getHeroesData());
    }
    public function overviewAction() {
      // echo stripos('http://localhost/dota2buff/heroes/1','/heroes');
      // exit;
      $hero = $this->getHeroesDetail($this->dispatcher->getParam('heroesid'));
      // return $this->response->setJsonContent($hero);
      $this->view->setVar('hero',$hero);
    }
    public function getHeroTrends($filter=null){

      $url = "https://www.dotabuff.com/heroes/played".$filter;
      $client = new Crawl();
      $crawler = $client->request('GET', $url);
      $filters = $crawler->filter('div.select.hasSelection select option')->each(function ($select, $j){
       $value = $select->attr('value');
       $disabled = ($select->attr('disabled') ? 'disabled' : '');
       $text = $select->text();
       return (object) [
         'value' => $value,
         'text' => $text,
         'disabled' => $disabled,
       ];
      });
      $heroes = $crawler->filter('article table tbody tr')->each(function ($tr, $j){
       $hero_link = ltrim($tr->filter('td.cell-icon div.image-container a')->attr('href'),'/');
       $hero_name = $tr->filter('td.cell-icon div.image-container a img')->attr('title');
       $hero_image = "https://www.dotabuff.com".$tr->filter('td.cell-icon div.image-container a img')->attr('src');
       $matches = $tr->filter('td')->each(function ($td, $i){
         return $td->attr('data-value');
       });
       $match = (object) [
         'played' => number_format($matches[2]),
         'pick_rate' =>number_format($matches[3],2) ,
         'win_rate' => number_format($matches[4],2),
         'kda_ratio' => number_format($matches[5],2),
       ];
       return (object) [
         'link' => $hero_link,
         'name' => $hero_name,
         'image' => $hero_image,
         'match' => $match,
       ];
      });
      return (object) [
       'filters' => $filters,
       'heroes' => $heroes,
      ];
    }
    public function trendsAction() {
      // return $this->response->setJsonContent($this->request->get('filter'));
      $filter='';
      if ($this->request->get('filter')){
        $filter = '?date='.$this->request->get('filter');
      }
      $trends = $this->getHeroTrends($filter);
      $this->view->setVar('trends',$trends);
      $s_filter = $this->request->get('filter');
      $this->view->setVar('s_filter',$s_filter);
    }

}
