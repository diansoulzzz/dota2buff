<?php

class LogoutController extends ControllerBase
{

    public function indexAction()
    {
        $this->session->remove("auth");
        return $this->response->redirect('');
    }

}
