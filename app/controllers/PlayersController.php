<?php

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Phalcon\Mvc\Url;

class PlayersController extends ControllerBase
{
    public function getProfileData()
    {
        $url = new Url();
        $url->setBaseUri($this->config->steamApi->realm."/");
        $file_game_mode = file_get_contents($url->get('assets/json/game_mode.json')); // put the contents of the file into a variable
        $game_mode = json_decode($file_game_mode);
        $file_lobby_type = file_get_contents($url->get('assets/json/lobby_type.json')); // put the contents of the file into a variable
        $lobby_type = json_decode($file_lobby_type);
        // return $this->response->setJsonContent($lobby_type);
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/players/']);
        $promises = [
            'profile'  => $client->getAsync($this->dispatcher->getParam('accountid')),
            'wl'  => $client->getAsync($this->dispatcher->getParam('accountid')."/wl"),
            'recentMatches'  => $client->getAsync($this->dispatcher->getParam('accountid')."/recentMatches"),
            'rankings'  => $client->getAsync($this->dispatcher->getParam('accountid')."/rankings"),
            'totals'  => $client->getAsync($this->dispatcher->getParam('accountid')."/totals"),
            'peers'  => $client->getAsync($this->dispatcher->getParam('accountid')."/peers"),
            'heroes'  => $client->getAsync($this->dispatcher->getParam('accountid')."/heroes"),
            'heroStats'  => $client->getAsync("/api/heroStats/"),
            'histograms'  => $client->getAsync($this->dispatcher->getParam('accountid')."/histograms"."/".($this->dispatcher->getParam('type') ? $this->dispatcher->getParam('type') : 'kills')),
        ];

        $results = Promise\unwrap($promises);
        $results = Promise\settle($promises)->wait();
        $profile = (object) [
            'info' =>  json_decode($results['profile']['value']->getBody()),
            'wl' => json_decode($results['wl']['value']->getBody()),
            'recentMatches'  =>  json_decode($results['recentMatches']['value']->getBody()),
            'rankings'  =>  json_decode($results['rankings']['value']->getBody()),
            'totals'  =>  json_decode($results['totals']['value']->getBody()),
            'peers'  =>  json_decode($results['peers']['value']->getBody()),
            'heroes'  =>  json_decode($results['heroes']['value']->getBody()),
            'heroStats'  =>  json_decode($results['heroStats']['value']->getBody()),
            'histograms'  =>  json_decode($results['histograms']['value']->getBody()),
        ];
        foreach ($profile->recentMatches as $key => $value) {
            foreach ($profile->heroStats as $key1 => $value1) {
                if ($value->hero_id==$value1->id)
                {
                    $profile->recentMatches[$key]->hero = $value1;
                }
            }
            $profile->recentMatches[$key]->date = gmdate("Y-m-d\TH:i:s\Z", $value->start_time);
            $profile->recentMatches[$key]->durationString = gmdate("i:s", $value->duration);
            $profile->recentMatches[$key]->dateString = date('l, M d Y', strtotime(gmdate("Y-m-d\TH:i:s\Z", $value->start_time)));
            if($value->lane_role=="1") {
                $profile->recentMatches[$key]->lane_name = '/ Safe';
            } else if ($value->lane_role=="2") {
                $profile->recentMatches[$key]->lane_name = '/ Mid';
            } else if ($value->lane_role=="3") {
                $profile->recentMatches[$key]->lane_name = '/ Off';
            } else {
                $profile->recentMatches[$key]->lane_name = '';
            }
            if($value->player_slot>=128 && $value->player_slot<=255) {
                $profile->recentMatches[$key]->team_type = 'Dire';
            } else {
                $profile->recentMatches[$key]->team_type = 'Radiant';
            }
            if (($profile->recentMatches[$key]->team_type=='Radiant') && ($value->radiant_win)) {
                $profile->recentMatches[$key]->game_status = 'Win';
            } else if (($profile->recentMatches[$key]->team_type=='Dire') && (!$value->radiant_win)) {
                $profile->recentMatches[$key]->game_status = 'Win';
            } else {
                $profile->recentMatches[$key]->game_status = 'Lose';
            }
            // if (($profile->recentMatches[$key]->team_type=='Radiant') && ($value->radiant_win)) {
            //     $profile->recentMatches[$key]->game_status = 'Win';
            // } else {
            //     $profile->recentMatches[$key]->game_status = 'Lose';
            // }
            foreach ($game_mode as $key1 => $value1) {
                if($value->game_mode == $value1->id)
                {
                    $profile->recentMatches[$key]->game_mode_name = ucwords(str_replace('_',' ',str_replace('game_mode_','',$value1->name)));
                }
            }
            foreach ($lobby_type as $key1 => $value1) {
                if($value->lobby_type == $value1->id)
                {
                    $profile->recentMatches[$key]->lobby_type_name = ucwords(str_replace('_',' ',str_replace('lobby_type_','',$value1->name)));
                }
            }
        }
        foreach ($profile->peers as $key => $value) {
            $profile->peers[$key]->last_played_date = gmdate("Y-m-d\TH:i:s\Z", $value->last_played);
            $profile->peers[$key]->last_played_dateString = date('l, M d Y', strtotime(gmdate("Y-m-d\TH:i:s\Z", $value->last_played)));
        }
        foreach ($profile->heroes as $key => $value) {
            foreach ($profile->heroStats as $key1 => $value1) {
                if ($value->hero_id==$value1->id)
                {
                    $profile->heroes[$key]->hero = $value1;
                }
            }
            $profile->heroes[$key]->last_played_date = gmdate("Y-m-d\TH:i:s\Z", $value->last_played);
            $profile->heroes[$key]->last_played_dateString = date('l, M d Y', strtotime(gmdate("Y-m-d\TH:i:s\Z", $value->last_played)));
        }

        return $profile;
        // $this->view->setVar('profile',$profile);
    }
    public function overviewAction()
    {
        // return $this->response->setJsonContent($this->getProfileData());
        $this->view->setVar('profile',$this->getProfileData());
        // echo gmdate("i:s", '2637');
        // exit;
        // $profile = "https://api.opendota.com/api/players/".$this->dispatcher->getParam('accountid');
        // $wl = "https://api.opendota.com/api/players/".$this->dispatcher->getParam('accountid')."/wl";
        // echo $this->dispatcher->getParam('accountid');
        // exit;
        // echo $this->config->application->baseUri;
        // exit;
    }
    public function matchesAction()
    {
        $this->view->setVar('profile',$this->getProfileData());
    }
    public function heroesAction()
    {
        $this->view->setVar('profile',$this->getProfileData());
    }
    public function histogramsAction()
    {
        $profile = $this->getProfileData();
        // $pluck = array_column($profile->histogramsKills, 'x');
        // $pluck = array_column($profile->histogramsKills, 'games');
        // $pluck = array_column($profile->histogramsKills, 'win');

        // return $this->response->setJsonContent($pluck);
        // return $this->response->setJsonContent($profile);
        $this->view->setVar('profile',$profile);
    }
}
