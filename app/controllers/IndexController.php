<?php

use GuzzleHttp\Client;
use Goutte\Client as Crawl;
use Phalcon\Mvc\Url;

class IndexController extends ControllerBase
{
    public function getRecent()
    {
        $url = "https://www.dotabuff.com/";
        $client = new Crawl();
        $crawler = $client->request('GET', $url);
        $recent = $crawler->filter('div.content-inner .col-8 .home-blog-post')->each(function ($tr, $i) {
          $news_name = $tr->filter('header a')->text();
          $news_time = ($tr->filter('header time')->count()) ? $tr->filter('header time')->text() : 'Today';
          // $news_time = $tr->filter('header time')->text();
          $news_image = $tr->filter('article div.image img')->attr('src');
          $news_summary = $tr->filter('article div.summary p')->text();
          $news = (object) [
            'news_name' => $news_name,
            'news_image' => $news_image,
            'news_summary' => $news_summary,
            'news_time' => $news_time,
          ];
          return $news;
        });
        $hero = $crawler->filter('div.content-inner .home-hero-pulse table tbody tr')->each(function ($tr, $i) {
          $hero_name = $tr->filter('div.image-container img.image-hero')->attr('alt');
          $hero_image = $tr->filter('div.image-container img.image-hero')->attr('src');

          $hero_rate = $tr->filter('td')->each(function ($td, $i) {
            if ($i==2) {
              return $td->text();
            }
            return false;
          });
          // $hero_win_rate = $tr->filter('div.image-container img.image-hero')->attr('src');
          $hero = (object) [
            'hero_name' => $hero_name,
            'hero_image' => 'https://www.dotabuff.com'.$hero_image,
            'hero_win_rate' => $hero_rate[2],
          ];
          return $hero;
        });
        //SORTING DATA
        usort($hero, function($a, $b) {
            return $b->hero_win_rate <=> $a->hero_win_rate;
        });
        return (object)[
          'news' => $recent,
          'hero' => $hero,
        ];
    }
    public function getRecentMatches()
    {
        $url = "https://www.dotabuff.com/esports/matches";
        $client = new Crawl();
        // $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 60);
        $crawler = $client->request('GET', $url);
        $match = $crawler->filter('table.recent-esports-matches tbody tr')->each(function ($tr, $i) {
            $match_image = $tr->filter('td a img.img-league')->attr('src');
            $match_name = $tr->filter('td a img.img-league')->attr('alt');
            $match_matches = $tr->filter('td')->each(function ($td, $i) {
              if ($i==2) {
                return (object)[
                  'id' => $td->filter('a')->text(),
                  'time_date' => $td->filter('time')->attr('datetime'),
                  'time_text' => $td->filter('time')->text(),
                ];
              } else if ($i==4) {
                $team_image = ($td->filter('span.team-image img')->count()) ? $td->filter('span.team-image img')->attr('src') : '';
                if ($team_image) {
                  if (!filter_var($team_image, FILTER_VALIDATE_URL)) {
                    $team_image = 'https://www.dotabuff.com'.$team_image;
                  }
                } else {
                  $url = new Url();
                  $url->setBaseUri($config->application->baseUri);
                  $team_image = $url->get('assets/images/icon2.png');
                }
                return (object)[
                  'team_image' => $team_image,
                  'team_name' => $td->filter('span.team-text')->text(),
                ];
              } else if ($i==5) {
                $team_image = ($td->filter('span.team-image img')->count()) ? $td->filter('span.team-image img')->attr('src') : '';
                if ($team_image) {
                  if (!filter_var($team_image, FILTER_VALIDATE_URL)) {
                    $team_image = 'https://www.dotabuff.com'.$team_image;
                  }
                } else {
                  $url = new Url();
                  $url->setBaseUri($config->application->baseUri);
                  $team_image = $url->get('assets/images/icon2.png');
                }
                return (object)[
                  'team_image' => $team_image,
                  'team_name' => $td->filter('span.team-text')->text(),
                ];
              }
              return false;
            });
            // return $match_matches;
            $match = (object) [
              'match_image' => $match_image,
              'match_name' => $match_name,
              'match_stats' => $match_matches[2],
              'match_team_win' => $match_matches[4],
              'match_team_lost' => $match_matches[5],
            ];
            return $match;
        });
        return $match;
    }
    public function indexAction()
    {
        $results = (object) [
          'recent_match' => $this->getRecentMatches(),
          'recent' => $this->getRecent(),
        ];
        // return $this->response->setJsonContent($results);
        $this->view->setVar('home', $results);
    }
}
