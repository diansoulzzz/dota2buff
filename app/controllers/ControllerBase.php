<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected $protected_bits = '00000001000100000000000000000001';
    // public function styles() {
    //     $this->assets->addCss('assets/css/style.css');
    //     $this->assets->addCss('assets/css/index.css');
    // }
    //
    // public function scripts() {
    //     $this->assets->addJs('assets/js/jquery.js');
    //     $this->assets->addJs('assets/js/bootstrap.min.js');
    // }
    public function steamAuth()
    {
        $steamAuth = new \SteamAuth\SteamOpenId([
            'realm' => $this->config->steamApi->realm,
            'return_to' => $this->config->steamApi->return_to,
        ]);
        return $steamAuth;
    }
    public function steamApi()
    {
        $steamApi = new \SteamAuth\SteamApi($this->config->steamApi->apiKey);
        return $steamApi;
    }
    public function afterExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher) {
        $this->view->core->steam->loginurl = $this->steamAuth()->getRedirectUrl();
    }

    function GET_32_BIT ($ID_64) {
        $upper = gmp_mul( bindec($this->protected_bits) , "4294967296" );
        return gmp_strval(gmp_sub($ID_64,$upper));
    }
    function MAKE_64_BIT ( $ID_32, $hi = false ) {
        if ($hi === false) {
            $hi = bindec($this->protected_bits);
        }
        $hi = sprintf ( "%u", $hi );
        $ID_32 = sprintf ( "%u", $ID_32 );
        return gmp_strval ( gmp_add ( gmp_mul ( $hi, "4294967296" ), $ID_32 ) );
    }
}
