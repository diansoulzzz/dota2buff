<?php
class VerifyController extends ControllerBase
{
    public function indexAction()
    {
        // return $this->response->setJsonContent($this->request->get());
        $steamAuth = $this->steamAuth();
        $verify = $this->request->get();
        if ($verify)
        {
            $steamid = $steamAuth->verifyAssertion($verify);
            $api = $this->steamApi();
            if($steamid)
            {
                $user = $api->getProfile($steamid);
                return $this->response->setJsonContent($user);
                return $this->response->redirect('');
            }
        }
    }

    private function _registerSession($user)
    {
        $this->session->set(
            "auth",
            [
                "steamid"   => $user->steamid,
                "personaname" => $user->personaname,
                "accountid" => $this->GET_32_BIT($user->steamid)
            ]
        );
    }


    public function callbackAction()
    {
        $steamAuth = $this->steamAuth();
        $verify = $this->request->get();
        $steamid = $steamAuth->verifyAssertion($verify);
        $api = $this->steamApi();
        if(!$steamid)
        {
            return $this->response->redirect('');
        }
        $steamUser = (object) $api->getProfile($steamid);
        // return $this->response->setJsonContent($steamUser);
        // exit;
        $user = Users::findFirst(
            [
                "steamid = :steamid:",
                "bind" => [
                    "steamid" => $steamUser->steamid,
                ],
            ]
        );
        if (!$user)
        {
            $newuser = new Users();
            $newuser->steamid= $steamUser->steamid;
            $newuser->personaname = "".$steamUser->personaname;
            $newuser->save();
            $user = $newuser;
        }
        $this->_registerSession($user);
        return $this->response->redirect('');
        // return $this->dispatcher->forward(
        //     [
        //         "controller" => "session",
        //         "action"     => "index",
        //     ]
        // );
    }
}
