<?php

use GuzzleHttp\Client;
// use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Goutte\Client as Crawl;

class ProPlayersController extends ControllerBase
{

    public function getProPlayerData()
    {
         $url = "https://api.opendota.com/api/proPlayers";
         $client = new Client();
         $response = $client->request('GET', $url);
         $data = json_decode($response->getBody());
         return $data;
    }
    public function getPlayerList()
    {
         $url = "https://www.dotabuff.com/players/winning";
         $client = new Crawl();
         $crawler = $client->request('GET', $url);
         $players = $crawler->filter('div .content-inner tbody tr')->each(function ($crawl, $i) {
           $rank = $crawl->filter('td.r-none-mobile.cell-centered')->html();
           $personname = $crawl->filter('td.cell-icon img')->attr('alt');
           $avatarfull = $crawl->filter('td.cell-icon img')->attr('src');
           $account_id = $crawl->filter('td.cell-icon a')->attr('href');
           $status = $crawl->filter('td')->each(function ($stats, $i) {
             if ($i==2){
               return $stats->filter('div.subtext.minor a time')->attr('datetime');
             }
             return $stats->attr('data-value');
           });
           $lastplayed = $status[2];
           // $lastplayed = date_diff($status[2], date("Y-m-d H:i:s"));
           $winrate = round($status[3],2);
           $matchesplayed = number_format($status[4]);
           $kdaratio = round($status[5],2);
           $timeplayed = $status[6];
           $player = (object) [
             'rank' => $rank,
             'personname' => $personname,
             'avatarfull' => $avatarfull,
             'account_id' => $account_id,
             'lastplayed' => $lastplayed,
             'winrate' => $winrate,
             'kdaratio' => $kdaratio,
             'matchesplayed' => $matchesplayed,
             'timeplayed' => $timeplayed,
           ];
           return $player;
         });
         return $players;
    }
    public function indexAction()
    {
        // $pro_players = $this->getProPlayerData();
        $pro_players = $this->getPlayerList();
        // return $this->response->setJsonContent($pro_players);
        $this->view->setVar('pro_players',$pro_players);
    }


}
