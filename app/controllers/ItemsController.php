<?php

use Goutte\Client as Crawl;

class ItemsController extends ControllerBase
{

    public function indexAction() {
        $url = "https://www.dotabuff.com/items/";
        $client = new Crawl();
        $crawler = $client->request('GET', $url);
        $table = $crawler->filter('tbody')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                if ($i==0) {
                    return [
                        $td->filter('img')->attr('src'),
                        $td->filter('img')->attr('data-tooltip-url'),
                        substr($td->filter('a')->attr('href'),1),
                    ];
                }
                return trim($td->text());
            });
        });
        // return $this->response->setJsonContent($table);
        $this->view->setVar('items',$table);
    }
    public function getItemDetail() {
      $itemslink = $this->dispatcher->getParam('itemslink');
      $url = "https://www.dotabuff.com/items/".$itemslink;
      $client = new Crawl();
      $crawler = $client->request('GET', $url);
      $filters = $crawler->filter('div.portable-show-item-details-default');
      $item_name = $filters->filter('article div.name')->text();
      $item_image =  "https://www.dotabuff.com/".$filters->filter('article div.avatar img')->attr('src');
      $item_link =  $filters->filter('article div.avatar a')->attr('href');
      $item_price_icon = $filters->filter('article div.price span.gold-icon img')->attr('src');
      $item_price_value = $filters->filter('article div.price span.value span.number')->text();
      $item_price = (object) [
        'value' => $item_price_value,
        'icon' => "https://www.dotabuff.com/".$item_price_icon,
      ];
      $item_stats = $filters->filter('article div.stats div.stat.attribute')->each(function ($stats,$i){
        return (object) [
          'ability' => $stats->filter('span.label')->text(),
          'value' => $stats->filter('span.value')->text(),
        ];
      });
      $item_shop = $filters->filter('article div.shop')->each(function ($img,$i){
        return (object)[
          'name' => $img->filter('img')->attr('alt'),
          'icon' => "https://www.dotabuff.com/".$img->filter('img')->attr('src'),
        ];
      });
      $item_description = $filters->filter('div.description div.description-block')->each(function ($desc,$i){
        return $desc->html();
      });
      $item_builds_from = $filters->filter('div.item-build.item-builds-from div.order div.item')->each(function ($from,$i){
        return (object)[
          'name' => $from->filter('div.icon img')->attr('alt'),
          'cost' => $from->filter('div.cost span.number')->text(),
          'image' => "https://www.dotabuff.com/".$from->filter('div.icon img')->attr('src'),
          'link' => ltrim($from->filter('div.icon a')->attr('href'),'/'),
        ];
      });
      $item_builds_into = $filters->filter('div.item-build.item-builds-into div.order div.item')->each(function ($from,$i){
        return (object)[
          'name' => $from->filter('div.icon img')->attr('alt'),
          'cost' => $from->filter('div.cost span.number')->text(),
          'image' => "https://www.dotabuff.com/".$from->filter('div.icon img')->attr('src'),
          'link' => ltrim($from->filter('div.icon a')->attr('href'),'/'),
        ];
      });
      $item_builds = (object) [
        'from' => $item_builds_from,
        'into' => $item_builds_into,
      ];
      $item_details = (object) [
        'item_name' => $item_name,
        'item_image' => $item_image,
        'item_link' => $item_link,
        'item_shop' => $item_shop,
        'item_price' => $item_price,
        'item_stats' => $item_stats,
        'item_description' => $item_description,
        'item_builds' => $item_builds,
      ];

      return $item_details;
    }
    public function overviewAction(){
      $detail = $this->getItemDetail();
      //return $this->response->setJsonContent($detail);
      $this->view->setVar('detail',$detail);
    }

}
