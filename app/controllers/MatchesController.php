<?php

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Goutte\Client as Crawl;
use Phalcon\Mvc\Url;

class MatchesController extends ControllerBase
{
  public function getMatchDetail() {

      $match_id = $this->dispatcher->getParam('matchid');
      $url = "https://www.dotabuff.com/matches/".$match_id;
      $client = new Crawl();
      $crawler = $client->request('GET', $url);
      $radiant_team = $crawler->filter('div.team-results section.radiant table tbody tr')->each(function ($tr, $i) {
        $hero_image = $tr->filter('td div.image-container-hero a img.image-hero')->attr('src');
        $hero_name = $tr->filter('td div.image-container-hero a img.image-hero')->attr('title');
        $hero_level = $tr->filter('td div.image-container-hero a span.overlay-text.bottom')->text();

        $user_name = ($tr->filter('td.tf-pl.single-lines a')->count()) ? ltrim($tr->filter('td.tf-pl.single-lines a')->html()) : ltrim($tr->filter('td.tf-pl.single-lines')->html());
        $pos = strpos($user_name, '<br>');
        if ($pos)
        {
          $user_name = substr($user_name,0,-(strlen($user_name)-$pos));
        }
        $uri = new Url();
        $uri->setBaseUri($config->application->baseUri);
        $user_link = ($tr->filter('td.tf-pl.single-lines a.link-type-player')->count()) ? $uri->get(substr($tr->filter('td.tf-pl.single-lines a.link-type-player')->attr('href'), 1)) : '';
        $user_photo = ($tr->filter('td.col-exclude.r-none-mobile div.image-container-player a img.image-player')->count() ? $tr->filter('td.col-exclude.r-none-mobile div.image-container-player a img.image-player')->attr('src') : '');
        $user = (object) [
          'name' => $user_name,
          'link' => $user_link,
          'photo' => $user_photo,
        ];
        $hero = (object) [
          'image' => 'https://www.dotabuff.com'.$hero_image,
          'name' => $hero_name,
          'level' => $hero_level,
        ];
        $item_builds = $tr->filter('td.r-group-4 div.match-item-with-time div.image-container')->each(function ($item_build, $i) {
          // return $item_build->html();
          // $item_name  = $item_build->filter('div.image-container-item a img.image-item')->attr('title');
          // $item_image  = $item_build->filter('div.image-container-item a img.image-item')->attr('src');
          $item_name  = $item_build->filter('img')->attr('title');
          $item_image  = $item_build->filter('img')->attr('src');
          return (object) [
            'name' => $item_name,
            'image' => 'https://www.dotabuff.com'.$item_image,
          ];
        });
        $stats = $tr->filter('td.r-tab')->each(function ($stat, $i) {
          if($i==0){
            return [
              'kill' => $stat->text(),
            ];
          } else if ($i==1){
            return [
              'death' => $stat->text(),
            ];
          } else if ($i==2){
            return [
              'assist' => $stat->text(),
            ];
          } else if ($i==3){
            return [
              'net_worth' => $stat->text(),
            ];
          } else if ($i==4){
            return [
              'last_hit' => $stat->text(),
            ];
          } else if ($i==6){
            return [
              'denied' => $stat->text(),
            ];
          } else if ($i==7){
            return [
              'gold_per_min' => $stat->text(),
            ];
          } else if ($i==9){
            return [
              'exp_per_min' => $stat->text(),
            ];
          } else if ($i==10){
            return [
              'damage_total' => $stat->text(),
            ];
          } else if ($i==11){
            return [
              'heal_total' => $stat->text(),
            ];
          } else if ($i==12){
            return [
              'damage_building_total' => $stat->text(),
            ];
          }
          return false;
        });
        $status = (object)[];
        foreach ($stats as $key => $value) {
          foreach ($value as $key2 => $value2) {
            $status->$key2 = $value2;
          }
        }
        return (object) [
          'hero' => $hero,
          'user' => $user,
          'item_builds' => $item_builds,
          'stats' => $status,
        ];
      });
      $radiant_ban = $crawler->filter('div.team-results section.radiant footer.r-none div.picks-inline div.ban')->each(function ($tr, $i) {
        $hero_image = $tr->filter('div.image-container-hero a img.image-hero')->attr('src');
        $hero_name = $tr->filter('div.image-container-hero a img.image-hero')->attr('alt');
        $hero_order = $tr->filter('div.seq')->text();
        $hero = (object) [
          'order' => $hero_order,
          'image' => 'https://www.dotabuff.com'.$hero_image,
          'name' => $hero_name,
        ];
        return $hero;
        // return (object) [
        //   'hero' => $hero,
        // ];
      });
      $radiant = (object) [
        'team' => $radiant_team,
        'ban' => $radiant_ban,
      ];
      // return $radiant;
      // DIRE
      $dire_team = $crawler->filter('div.team-results section.dire table tbody tr')->each(function ($tr, $i) {
        $hero_image = $tr->filter('td div.image-container-hero a img.image-hero')->attr('src');
        $hero_name = $tr->filter('td div.image-container-hero a img.image-hero')->attr('title');
        $hero_level = $tr->filter('td div.image-container-hero a span.overlay-text.bottom')->text();

        $user_name = ($tr->filter('td.tf-pl.single-lines a')->count()) ? ltrim($tr->filter('td.tf-pl.single-lines a')->html()) : ltrim($tr->filter('td.tf-pl.single-lines')->html());
        $pos = strpos($user_name, '<br>');
        if ($pos)
        {
          $user_name = substr($user_name,0,-(strlen($user_name)-$pos));
        }
        $uri = new Url();
        $uri->setBaseUri($config->application->baseUri);
        $user_link = ($tr->filter('td.tf-pl.single-lines a.link-type-player')->count()) ? $uri->get(substr($tr->filter('td.tf-pl.single-lines a.link-type-player')->attr('href'), 1)) : '';
        $user_photo = ($tr->filter('td.col-exclude.r-none-mobile div.image-container-player a img.image-player')->count() ? $tr->filter('td.col-exclude.r-none-mobile div.image-container-player a img.image-player')->attr('src') : '');
        $user = (object) [
          'name' => $user_name,
          'link' => $user_link,
          'photo' => $user_photo,
        ];
        $hero = (object) [
          'image' => 'https://www.dotabuff.com'.$hero_image,
          'name' => $hero_name,
          'level' => $hero_level,
        ];
        $item_builds = $tr->filter('td.tf-pl.r-tab div.match-item-with-time div.image-container')->each(function ($item_build, $i) {
          // return $item_build->html();
          // $item_name  = $item_build->filter('div.image-container-item a img.image-item')->attr('title');
          // $item_image  = $item_build->filter('div.image-container-item a img.image-item')->attr('src');
          $item_name  = $item_build->filter('img')->attr('title');
          $item_image  = $item_build->filter('img')->attr('src');
          return (object) [
            'name' => $item_name,
            'image' => 'https://www.dotabuff.com'.$item_image,
          ];
        });
        $stats = $tr->filter('td.r-tab')->each(function ($stat, $i) {
          if($i==0){
            return [
              'kill' => $stat->text(),
            ];
          } else if ($i==1){
            return [
              'death' => $stat->text(),
            ];
          } else if ($i==2){
            return [
              'assist' => $stat->text(),
            ];
          } else if ($i==3){
            return [
              'net_worth' => $stat->text(),
            ];
          } else if ($i==4){
            return [
              'last_hit' => $stat->text(),
            ];
          } else if ($i==6){
            return [
              'denied' => $stat->text(),
            ];
          } else if ($i==7){
            return [
              'gold_per_min' => $stat->text(),
            ];
          } else if ($i==9){
            return [
              'exp_per_min' => $stat->text(),
            ];
          } else if ($i==10){
            return [
              'damage_total' => $stat->text(),
            ];
          } else if ($i==11){
            return [
              'heal_total' => $stat->text(),
            ];
          } else if ($i==12){
            return [
              'damage_building_total' => $stat->text(),
            ];
          }
          return false;
        });
        $status = (object)[];
        foreach ($stats as $key => $value) {
          foreach ($value as $key2 => $value2) {
            $status->$key2 = $value2;
          }
        }
        return (object) [
          'hero' => $hero,
          'user' => $user,
          'item_builds' => $item_builds,
          'stats' => $status,
        ];
      });
      $dire_ban = $crawler->filter('div.team-results section.dire footer.r-none div.picks-inline div.ban')->each(function ($tr, $i) {
        $hero_image = $tr->filter('div.image-container-hero a img.image-hero')->attr('src');
        $hero_name = $tr->filter('div.image-container-hero a img.image-hero')->attr('alt');
        $hero_order = $tr->filter('div.seq')->text();
        $hero = (object) [
          'order' => $hero_order,
          'image' => 'https://www.dotabuff.com'.$hero_image,
          'name' => $hero_name,
        ];
        return $hero;
        // return (object) [
        //   'hero' => $hero,
        // ];
      });
      $dire = (object) [
        'team' => $dire_team,
        'ban' => $dire_ban,
      ];

      $players = [];
      foreach ($radiant->team as $key => $value) {
        $players[] = $value;
      }
      foreach ($dire->team as $key => $value) {
        $players[] = $value;
      }
      $t_k = 0;
      $t_a = 0;
      $t_d = 0;
      foreach ($players as $key => $value) {
        $t_k += $value->stats->kill;
        $t_a += $value->stats->assist;
        $t_d += $value->stats->death;
      }

      $m_k = 0;
      $m_a = 0;
      $m_d = 0;
      $is_mvp_kill = 0;
      $is_mvp_death = 0;
      $is_mvp_assist = 0;
      foreach ($players as $key => $value) {
        if ($m_k < $value->stats->kill){
          $m_k = $value->stats->kill;
          $is_mvp_kill = $key;
        }
        if ($m_a < $value->stats->assist){
          $m_a = $value->stats->assist;
          $is_mvp_assist = $key;
        }
        if ($m_d < $value->stats->death){
          $m_d = $value->stats->death;
          $is_mvp_death = $key;
        }
      }
      foreach ($players as $key => $value) {
        if ($is_mvp_kill == $key) {
          $players[$key]->mvp_kill = true;
        } else {
          $players[$key]->mvp_kill = false;
        }
        if ($is_mvp_death == $key) {
          $players[$key]->mvp_death = true;
        } else {
          $players[$key]->mvp_death = false;
        }
        if ($is_mvp_assist == $key) {
          $players[$key]->mvp_assist = true;
        } else {
          $players[$key]->mvp_assist = false;
        }
      }
      foreach ($players as $key => $value) {
        $overall = (object) [
          'kill' => round((float)($value->stats->kill*100)/$t_k).'%',
          'assist' => round((float)($value->stats->assist*100)/$t_a).'%',
          'death' => round((float)($value->stats->death*100)/$t_d).'%',
        ];
        $players[$key]->overall = $overall;
      }
      $total = (object) [
        'total_kill' => $t_k,
        'total_assist' => $t_a,
        'total_death' => $t_d,
      ];

      // $advantage_graph = $crawler->filter('section.team-advantages')->each(function ($tr, $i) {
      //
      // });
      // return $advantage_graph;
      return (object) [
        'radiant' => $radiant,
        'dire' => $dire,
        'players' => $players,
        'total' => $total,
      ];
  }
  public function getMatchesData() {
      $url = new Url();
      $url->setBaseUri($this->config->steamApi->realm."/");
      $file_game_mode = file_get_contents($url->get('assets/json/game_mode.json')); // put the contents of the file into a variable
      $game_mode = json_decode($file_game_mode);
      $file_lobby_type = file_get_contents($url->get('assets/json/lobby_type.json')); // put the contents of the file into a variable
      $lobby_type = json_decode($file_lobby_type);
      $file_items = file_get_contents($url->get('assets/json/items.json')); // put the contents of the file into a variable
      $items = json_decode($file_items);
      // return $this->response->setJsonContent($lobby_type);
      $client = new Client(['base_uri' => 'https://api.opendota.com/api/matches/']);
      $promises = [
          '/'  => $client->getAsync($this->dispatcher->getParam('matchid')),
          'heroStats'  => $client->getAsync("/api/heroStats/"),
      ];

      $results = Promise\unwrap($promises);
      $results = Promise\settle($promises)->wait();
      $matches = (object) [
          'match' =>  json_decode($results['/']['value']->getBody()),
          'heroStats'  =>  json_decode($results['heroStats']['value']->getBody()),
      ];
      // return $matches;
      // 128-255

      foreach ($matches->match->players as $key => $value) {
        foreach ($matches->heroStats as $key1 => $value1) {
          if ($value->hero_id==$value1->id)
          {
              $matches->match->players[$key]->hero = $value1;
          }
        }
      }

      $radiant_team = [];
      $dire_team = [];
      foreach ($matches->match->players as $key => $value) {
        $combat_log = [];
        if($value->player_slot>=128 && $value->player_slot<=255) {
           $dire_team[] = $value;
        } else {
           $radiant_team[] = $value;
        }
        foreach ($value->kills_log as $key1 => $kill_log) {
          $time = gmdate("i:s", $kill_log->time);
          $key = ucwords(str_replace('_',' ',str_replace('npc_dota_hero_','',$kill_log->key)));
          // str_replace("world","Peter","Hello world!");
          $combat_log[] = (object)[
            'time' => $time,
            'key' => $key,
          ];
        }
        $value->combat_log = $combat_log;
      }

      $matches->match->radiant_team = $radiant_team;
      $matches->match->dire_team = $dire_team;

      $matches->match->durationString = gmdate("i:s", $matches->match->duration);
      foreach ($game_mode as $key1 => $value1) {
          if($matches->match->game_mode == $value1->id)
          {
              $matches->match->game_mode_name = ucwords(str_replace('_',' ',str_replace('game_mode_','',$value1->name)));
          }
      }
      foreach ($lobby_type as $key1 => $value1) {
          if($matches->lobby_type == $value1->id)
          {
              $matches->match->lobby_type_name = ucwords(str_replace('_',' ',str_replace('lobby_type_','',$value1->name)));
          }
      }


      return $matches;
  }
  public function overviewAction() {
      $matches = $this->getMatchesData();
      $detail = $this->getMatchDetail();
      // return $this->response->setJsonContent($matches);
      $this->view->setVar('matches',$matches);
      $this->view->setVar('detail',$detail);
  }
}
