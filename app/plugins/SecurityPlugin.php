<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Role;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Resource;

class SecurityPlugin extends Plugin
{
    public function getAcl()
    {
        $acl = new AclList();
        $acl->setDefaultAction(
            Acl::DENY
        );
        $roles = [
            "users"  => new Role("Users"),
            "guests" => new Role("Guests"),
        ];
        foreach ($roles as $role) {
            $acl->addRole($role);
        }
        $privateResources = [
            "profile" => ['index'],
            "logout" => ['index'],
        ];

        foreach ($privateResources as $resourceName => $actions) {
            $acl->addResource(
                new Resource($resourceName),
                $actions
            );
        }
        $publicResources = [
            "index" => ["index"],
            "players" => ['overview'],
            "matches" => ['overview'],
            "pro-players" => ["index"],
            "heroes" => ["index"],
            "items" => ["index"],
            "verify" => ["callback"],
            "about" => ["index"],
            "errors" => ["show404", "show500"],
            "abcd" => ["index"],
            "story" => ["index"],
            "event" => ["index"],
            "schedule" => ["index"],
        ];

        foreach ($publicResources as $resourceName => $actions) {
            $acl->addResource(
                new Resource($resourceName),
                $actions
            );
        }

        foreach ($roles as $role) {
            foreach ($publicResources as $resource => $actions) {
                $acl->allow(
                    $role->getName(),
                    $resource,
                    "*"
                );
            }
        }

        foreach ($privateResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow(
                    "Users",
                    $resource,
                    $action
                );
            }
        }
        return $acl;
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get("auth");
        if (!$auth) {
            $role = "Guests";
        } else {
            $role = "Users";
        }
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        // echo $role." ".$controller." ".$action;
        // exit;
        // Obtain the ACL list
        $acl = $this->getAcl();
        // Check if the Role have access to the controller (resource)
        $allowed = $acl->isAllowed($role, $controller, $action);
        // echo $role." ".$controller." ".$action." ".$allowed;
        // exit;
        if (!$allowed) {
            // If he doesn't have access forward him to the index controller
            $this->flash->error(
                "You need login first to continue."
            );
            // $dispatcher->forward(
            //     [
            //         "controller" => "index",
            //         "action"     => "index",
            //     ]
            // );
            // ;
            return $this->response->redirect('');
        }
    }
}
