{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-12">
        <div class="card">
          <div class="card__header card__header--has-btn">
            <h4>Dota 2 Schedule</h4>
          </div>
          <div class="widget__content card__content">
            <ul class="newslog">

              <li class="newslog__item">
                <h4 style="float:left">Schedule </h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">
                  No matches are currently scheduled. Please check back later.
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<style>
.newslog__content{
  font-size: 15px;

}
</style>
{% endblock %}
