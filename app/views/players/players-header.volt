<div class="player-heading">
  <div class="container">
    {# <div class="player-info__team-logo">
      <img src="{{profile.info.profile.avatarfull}}" alt="">
    </div> #}
    <div class="player-info__title player-info__title--mobile">
      {# <div class="player-info__number">{{profile.info.rank_tier}}</div> #}
      <h1 class="player-info__name">
        <span class="player-info__first-name">{{profile.info.profile.name}}</span>
        <span class="player-info__last-name">{{profile.info.profile.personaname}}</span>
      </h1>
    </div>
    <div class="player-info">
      <div class="player-info__item player-info__item--photo">
          <figure class="player-info__photo">
            <img style="height: 300px;display: block;margin-left: auto;margin-right: auto;margin-top: 5%;" src="{{profile.info.profile.avatarfull}}" alt="">
          </figure>
      </div>
      <div class="player-info__item player-info__item--details">
        <div class="player-info__title player-info__title--desktop">
          {# <div class="player-info__number">{{profile.info.rank_tier}}</div> #}
          <h1 class="player-info__name">
            <span class="player-info__first-name">{{profile.info.profile.name}}  <a style="color:white" href="{{ profile.info.profile.profileurl }}"><i class="fa fa-steam"></i></a></span>
            <span class="player-info__last-name">{{profile.info.profile.personaname}}</span>
          </h1>
        </div>
        <div class="player-info-details">
          <div class="player-info-details__item player-info-details__item--height">
            <h6 class="player-info-details__title">Wins</h6>
            <div class="player-info-details__value">{{profile.wl.win}}</div>
          </div>
          <div class="player-info-details__item player-info-details__item--weight">
            <h6 class="player-info-details__title">Losses</h6>
            <div class="player-info-details__value">{{profile.wl.lose}}</div>
          </div>
          <div class="player-info-details__item player-info-details__item--age">
            {# <h6 class="player-info-details__title">Winrate</h6>
            <div class="player-info-details__value">{{noformat((profile.wl.win / (profile.wl.win + profile.wl.lose)) * 100)}} %</div> #}
          </div>
          <div class="player-info-details__item player-info-details__item--college">
            <h6 class="player-info-details__title">Solor MMR</h6>
            <div class="player-info-details__value">{{profile.info.solo_competitive_rank}}</div>
          </div>
          <div class="player-info-details__item player-info-details__item--born">
            <h6 class="player-info-details__title">Rank Tier</h6>
            <div class="player-info-details__value">{{profile.info.rank_tier}}</div>
          </div>
          <div class="player-info-details__item player-info-details__item--position">
            <h6 class="player-info-details__title">Estimated MMR</h6>
            <div class="player-info-details__value">{{profile.info.mmr_estimate.estimate}}</div>
          </div>
        </div>
        <div class="player-info-stats">
          <div class="player-info-stats__item">
            <div class="circular">
              <div class="circular__bar" data-percent="{{noformat((profile.wl.win / (profile.wl.win + profile.wl.lose)) * 100)}}" data-track-color="#4c5d6f">
                <span class="circular__percents">{{noformat((profile.wl.win / (profile.wl.win + profile.wl.lose)) * 100)}}<small>percent</small></span>
              </div>
              <span class="circular__label"><strong>Winning</strong> rates</span>
            </div>
          </div>
          <div class="player-info-stats__item">
            <div class="circular">
              <div class="circular__bar" data-percent="{{noformat((profile.totals[4].sum / profile.totals[4].n) / 2000 * 100)}}" data-track-color="#4c5d6f">
                <span class="circular__percents">{{noformat(profile.totals[4].sum / profile.totals[4].n)}}<small>minutes</small></span>
              </div>
              <span class="circular__label"><strong>Gold</strong> rates</span>
            </div>
          </div>
          <div class="player-info-stats__item">
            <div class="circular">
              <div class="circular__bar" data-percent="{{noformat((profile.totals[5].sum / profile.totals[5].n) / 2000 * 100)}}" data-track-color="#4c5d6f">
                <span class="circular__percents">{{noformat(profile.totals[5].sum / profile.totals[5].n)}}<small>minutes</small></span>
              </div>
              <span class="circular__label"><strong>XP</strong> rates</span>
            </div>
          </div>
        </div>
      </div>
      <div class="player-info__item player-info__item--stats">
        <canvas id="players-statistics" class="player-info-chart" height="290"></canvas>
      </div>
    </div>
  </div>
</div>

<nav class="content-filter">
  <div class="container">
    <a href="#" class="content-filter__toggle"></a>
    <ul class="content-filter__list">
      <li class="content-filter__item {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id ? 'content-filter__item--active':'' }}">
          <a href="{{url('players/' ~ profile.info.profile.account_id)}}" class="content-filter__link"><small>Player</small>Overview</a>
      </li>
      <li class="content-filter__item {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id ~ '/matches' ? 'content-filter__item--active':'' }}">
          <a href="{{url('players/' ~ profile.info.profile.account_id ~ '/matches')}}" class="content-filter__link"><small>Player</small>Matches</a>
      </li>
      <li class="content-filter__item {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/heroes' ? 'content-filter__item--active':'' }}">
          <a href="{{url('players/' ~ profile.info.profile.account_id ~ '/heroes')}}" class="content-filter__link"><small>Player</small>Heroes</a>
      </li>
      <li class="content-filter__item {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/histograms' ? 'content-filter__item--active':'' }}">
          <a href="{{url('players/' ~ profile.info.profile.account_id ~ '/histograms')}}" class="content-filter__link"><small>Player</small>Histograms</a>
      </li>
    </ul>
  </div>
</nav>
