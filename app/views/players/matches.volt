{% extends 'layouts/default.volt' %}

{% block content %}
{% include "players/players-header.volt" %}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-12">
        <div class="card">
          <div class="card__header card__header--has-btn">
            <h4>Recent Matches</h4>
            {# <a href="player-stats.html" class="btn btn-default btn-outline btn-xs card-header__button">See complete games log</a> #}
          </div>
          <div class="card__content">
            <div class="table-responsive">
              <table id="dataTables" class="table table-hover game-player-result">
                <thead>
                  <tr>
                    <th class="game-player">#</th>
                    <th class="game-player-result__date">Date</th>
                    <th class="game-player-result__vs">Hero</th>
                    <th class="game-player">ID</th>
                    <th class="game-player-result__score">Result</th>
                    <th class="game-player">Duration</th>
                    <th class="game-player">Kill</th>
                    <th class="game-player">Death</th>
                    <th class="game-player">Assist</th>
                  </tr>
                </thead>
                <tbody>
                  {% for i, match in profile.recentMatches %}
                    <tr>
                      <td>{{i+1}}</td>
                      <td class="game-player-result__date">{{match.dateString}}</td>
                      <td class="game-player-result__vs">
                        <div class="team-meta">
                          <figure class="team-meta__logo">
                            <img src="{{url('https://api.opendota.com' ~ match.hero.img)}}" alt="">
                          </figure>
                          <div class="team-meta__info">
                            <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url('heroes/'~match.hero_id)}}">{{match.hero.localized_name}}</a></h6>
                            <span class="team-meta__place">{{match.team_type}} {{match.lane_name}}</span>
                          </div>
                        </div>
                      </td>
                      <td class="game-player">
                          <div class="team-meta__info">
                            <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url('matches/'~match.match_id)}}">{{match.match_id}}</a></h6>
                            <span class="team-meta__place">{{match.lobby_type_name}} / {{match.game_mode_name}}</span>
                          </div>
                      </td>
                      <td class="game-player-result__score">
                          <span class="game-player-result__game" style="{{(match.game_status)=='Lose'?'color:red':'color:green'}}">{{match.game_status}}</span>
                      </td>
                      <td class="game-player">{{match.durationString}}</td>
                      <td class="game-player">{{match.kills}}</td>
                      <td class="game-player">{{match.deaths}}</td>
                      <td class="game-player">{{match.assists}}</td>
                    </tr>
                  {% endfor %}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.table_init();
  },
  p_init: function ()
  {
      var radar_data = {
          type: 'radar',
          data: {
              labels: ["KILL", "DEATH", "ASSISTS", "KDA", "DENIES"],
              datasets: [{
                  data: [
                      {{noformat(profile.totals[0].sum / profile.totals[0].n)}},
                      {{noformat(profile.totals[1].sum / profile.totals[1].n)}},
                      {{noformat(profile.totals[2].sum / profile.totals[2].n)}},
                      {{noformat(profile.totals[3].sum / profile.totals[3].n)}},
                      {{noformat(profile.totals[7].sum / profile.totals[7].n)}}
                  ],
                  backgroundColor: "rgba(255,220,17,0.8)",
                  borderColor: "#ffdc11",
                  pointBorderColor: "rgba(255,255,255,0)",
                  pointBackgroundColor: "rgba(255,255,255,0)",
                  pointBorderWidth: 0
              }]
          },
          options: {
              legend: {
                  display: false,
              },
              tooltips: {
                  backgroundColor: "rgba(49,64,75,0.8)",
                  titleFontSize: 10,
                  titleSpacing: 2,
                  titleMarginBottom: 4,
                  bodyFontFamily: 'Montserrat, sans-serif',
                  bodyFontSize: 9,
                  bodySpacing: 0,
                  cornerRadius: 2,
                  xPadding: 10,
                  displayColors: false,
              },
              scale: {
                  angleLines: {
                      color: "rgba(255,255,255,0.025)",
                  },
                  pointLabels: {
                      fontColor: "#9a9da2",
                      fontFamily: 'Montserrat, sans-serif',
                  },
                  ticks: {
                      beginAtZero: true,
                      display: false,
                  },
                  gridLines: {
                      color: "rgba(255,255,255,0.05)",
                      lineWidth: 2,
                  },
                  labels: {
                      display: false
                  }
              }
          },
      };
      var ctx = $('#players-statistics');
      var playerInfo = new Chart(ctx, radar_data);
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
