{% extends 'layouts/default.volt' %}

{% block content %}
{% include "players/players-header.volt" %}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-12">
        <div class="card">
          <div class="card__header">
            <form class="form-inline">
              <div class="form-group">
               <label style="margin-right:10px">Select : </label>
               <select class="form-control input-sm" id="filter-histograms">
                 <option value="kills" {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/histograms/kills' ? 'selected':'' }}>Kills</option>
                 <option value="assists" {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/histograms/assists' ? 'selected':'' }}>Assists</option>
                 {# <option value="comeback" {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/histograms/comeback' ? 'selected':'' }}>Comebacks</option> #}
                 <option value="deaths" {{ router.getRewriteUri()=='/players/' ~ profile.info.profile.account_id  ~ '/histograms/deaths' ? 'selected':'' }}>Deaths</option>
               </select>
              </div>
            </form>
          </div>
          <div class="card__content">
              <canvas id="players-histograms" class="points-history-chart" height="135"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.h_init();
    thisform.filter_init();
  },
  p_init: function ()
  {
      var radar_data = {
          type: 'radar',
          data: {
              labels: ["KILL", "DEATH", "ASSISTS", "KDA", "DENIES"],
              datasets: [{
                  data: [
                      {{noformat(profile.totals[0].sum / profile.totals[0].n)}},
                      {{noformat(profile.totals[1].sum / profile.totals[1].n)}},
                      {{noformat(profile.totals[2].sum / profile.totals[2].n)}},
                      {{noformat(profile.totals[3].sum / profile.totals[3].n)}},
                      {{noformat(profile.totals[7].sum / profile.totals[7].n)}}
                  ],
                  backgroundColor: "rgba(255,220,17,0.8)",
                  borderColor: "#ffdc11",
                  pointBorderColor: "rgba(255,255,255,0)",
                  pointBackgroundColor: "rgba(255,255,255,0)",
                  pointBorderWidth: 0
              }]
          },
          options: {
              legend: {
                  display: false,
              },
              tooltips: {
                  backgroundColor: "rgba(49,64,75,0.8)",
                  titleFontSize: 10,
                  titleSpacing: 2,
                  titleMarginBottom: 4,
                  bodyFontFamily: 'Montserrat, sans-serif',
                  bodyFontSize: 9,
                  bodySpacing: 0,
                  cornerRadius: 2,
                  xPadding: 10,
                  displayColors: false,
              },
              scale: {
                  angleLines: {
                      color: "rgba(255,255,255,0.025)",
                  },
                  pointLabels: {
                      fontColor: "#9a9da2",
                      fontFamily: 'Montserrat, sans-serif',
                  },
                  ticks: {
                      beginAtZero: true,
                      display: false,
                  },
                  gridLines: {
                      color: "rgba(255,255,255,0.05)",
                      lineWidth: 2,
                  },
                  labels: {
                      display: false
                  }
              }
          },
      };
      var ctx = $('#players-statistics');
      var playerInfo = new Chart(ctx, radar_data);
  },
  h_init: function ()
  {
      var data = {
          type: 'bar',
          data: {
              labels: {{json_encode(array_column(profile.histograms,'x'))}},
              datasets: [{
                  label: 'match',
                  data: {{json_encode(array_column(profile.histograms,'games'))}},
                  backgroundColor: "rgba(255,220,17,0.8)",
                  borderColor: "#ffdc11",
                  pointBorderColor: "rgba(255,255,255,0)",
                  pointBackgroundColor: "rgba(255,255,255,0)",
                  pointBorderWidth: 0
              }]
          },
          options: {
              legend: {
                  display: false,
              },
              tooltips: {
                  backgroundColor: "rgba(49,64,75,0.8)",
                  titleFontSize: 10,
                  titleSpacing: 2,
                  titleMarginBottom: 4,
                  bodyFontFamily: 'Montserrat, sans-serif',
                  bodyFontSize: 9,
                  bodySpacing: 0,
                  cornerRadius: 2,
                  xPadding: 10,
                  displayColors: false,
              },
              scale: {
                  angleLines: {
                      color: "rgba(255,255,255,0.025)",
                  },
                  pointLabels: {
                      fontColor: "#9a9da2",
                      fontFamily: 'Montserrat, sans-serif',
                  },
                  ticks: {
                      beginAtZero: true,
                      display: false,
                  },
                  gridLines: {
                      color: "rgba(255,255,255,0.05)",
                      lineWidth: 2,
                  },
                  labels: {
                      display: false
                  }
              },
      				scales: {
      					xAxes: [{
      						display: true,
      						scaleLabel: {
      							display: true,
      							labelString: $('#filter-histograms').val(),
      						}
      					}],
      					yAxes: [{
      						display: true,
      						scaleLabel: {
      							display: true,
      							labelString: 'Match',
      						}
      					}]
      				}
          },
      };
      var ctx = $('#players-histograms');
      var playerInfo = new Chart(ctx, data);
  },
  filter_init : function()
  {
    $('#filter-histograms').on('change',function(){
      $url = "{{url('players/' ~ profile.info.profile.account_id ~ '/histograms')}}"+"/"+$(this).val();
      window.location.href = $url;
    })
  }
};
</script>
{% endblock %}
