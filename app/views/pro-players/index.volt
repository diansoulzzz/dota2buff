{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    {# <div class="card card--has-table"> #}
    <div class="card">
      <div class="card__header card__header--has-btn">
        <h4>Pro Player</h4>
      </div>
      <div class="card__content">
        <div class="table-responsive">
          <table id="dataTables" class="table table-hover team-result">
            <thead>
              <tr>
                <th class="team-standings__pos">#</th>
                <th class="team-result__vs">Player</th>
                <th class="team-result__status">Win Rate</th>
                <th class="team-result__points">Matches Played</th>
                {# <th class="team-result center">Time Played</th> #}
              </tr>
            </thead>
            <tbody>
              {% for i, player in pro_players %}
                  <tr>
                    <td>{{i}}</td>
                    <td class="team-result__vs">
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{url(player.avatarfull)}}" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url(player.account_id)}}">{{player.personname}}</a></h6>
                          {# <span class="team-meta__place timeago">{{player.lastplayed}}</span> #}
                        </div>
                      </div>
                    </td>
                    <td class="team-result__status">
                      <p style="margin:0;">{{player.winrate}}%</p>
                      <div class="bar-container">
                        <div class="percent green" style="width:{{player.winrate}};"></div>
                      </div>
                    </td>
                    <td class="team-result__points">{{player.matchesplayed}} times</td>
                    {# <td class="team-result center">{{player.timeplayed}}</td> #}
                  </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<style>
.mvp {
  border-radius: 25%;
  color:black;
  line-height: 30px;
  font-weight: bold;
  text-align: center;
  height: 30px;
  width: 70px;
  margin: auto;
}
.kill {
  background: rgba(255, 215, 0, 0.59);
  font-weight: bold;
  color:black;
}
.feed {
  background: rgba(255, 0, 0, 0.64);
}
.assist {
  background: rgba(0, 79, 255, 0.44);
}
.mvp.kill {
  background: rgb(255,215,0);
}
.mvp.feed {
  background: rgb(255, 0, 0);
}
.mvp.assist {
  background: rgb(0, 79, 255);
}
.bar-container {
  width: 100%;
  background-color: rgb(150, 150, 150);
  height: auto;
  margin: auto;
}
.bar-container .percent.green {
  background-color: rgb(50, 231, 5);
  height: 10px;
}
.bar-container .percent.blue {
  background: rgb(0, 79, 255);
  height: 10px;
}
.bar-container .percent.red {
  background-color: rgb(255, 0, 0);
  height: 10px;
}
.bar-container .percent.yellow {
  background-color: rgb(255,215,0);
  height: 10px;
}

.team-radiant {
  color: rgb(76, 237, 83) !important;
  fill: currentColor;
}
.team-radiant h5 {
  color: currentColor;
}
.team-win {
  filter: drop-shadow(rgb(255, 171, 64) 0px 0px 4px);
}
.team-dire {
  color: rgb(221, 38, 13) !important;
  fill: currentColor;
}
.team-dire h5 {
  color: currentColor;
}
.score-lose {
  color: rgb(139, 139, 139);
}
.score-win {
  color: rgb(242, 234, 33);
}
</style>
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('assets/vendor/timeago/timeago.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
