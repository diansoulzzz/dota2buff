{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    {# <div class="card card--has-table"> #}
    <div class="card">
      <div class="card__header card__header--has-btn">
        <h4>Pro Player</h4>
      </div>
      <div class="card__content">
        <div class="table-responsive">
          <table id="dataTables" class="table table-hover team-result">
            <thead>
              <tr>
                <th class="team-standings__pos">#</th>
                <th class="team-result__vs">Player</th>
                <th class="team-result__status">Team</th>
                <th class="team-result__points">Profile URL</th>
                <th class="team-result center">Country Code</th>
              </tr>
            </thead>
            <tbody>
              {% for i, player in pro_players %}
                  {% if i is 0 %}
                      {% continue %}
                  {% endif %}
                  <tr>
                    <td>{{i}}</td>
                    <td class="team-result__vs">
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{url(player.avatarfull)}}" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url('players/'~player.account_id)}}">{{player.name}}</a></h6>
                          <span class="team-meta__place">{{player.personaname}}</span>
                        </div>
                      </div>
                    </td>
                    <td class="team-result__status">{{player.team_name}}</td>
                    <td class="team-result__points"><a style="color:rgb(102, 187, 255)" href="{{player.profileurl}}">{{player.profileurl}}</a></td>
                    <td class="team-result center">{{player.country_code}}</td>
                  </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
