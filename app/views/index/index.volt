{% extends 'layouts/default.volt' %}

{% block content %}
<div class="modal" tabindex="-1" role="dialog" id="notifModal">
  <div class="modal-dialog" style="width:60%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="float:left">New Hero Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding:0px !important;">
        <img src="https://i.imgur.com/XNgHTaR.jpg" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>

<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-8">
        <div class="card card--clean">
          <header class="card__header card__header--has-btn">
            <h4>Recent News</h4>
            {# <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">See All Posts</a> #}
          </header>
          <div class="card__content">
            <div class="posts posts--cards posts--cards-thumb-left post-list">
              {% for i, recent_news in home.recent.news %}
                <div class="post-list__item">
                  <div class="posts__item posts__item--card posts__item--category-1 card">
                    <figure class="posts__thumb">
                      <a href="#"><img class="img-thumbnail" style="max-height:350px;width:100vh;height:100vh;" src="{{recent_news.news_image}}" alt="{{recent_news.news_name}}"></a>
                      {# <a href="#" class="posts__cta"></a> #}
                    </figure>
                    <div class="posts__inner">
                      <div class="card__content">
                        {# <div class="posts__cat">
                          <span class="label posts__cat-label">The Team</span>
                        </div> #}
                        <h6 class="posts__title"><a href="#">{{recent_news.news_name}}</a></h6>
                        <time datetime="{{recent_news.news_time}}" class="posts__date">{{recent_news.news_time}}</time>
                        <div class="posts__excerpt">
                          {{recent_news.news_summary}}
                        </div>
                      </div>
                      {# <footer class="posts__footer card__footer">
                        <div class="post-author">
                          <figure class="post-author__avatar">
                            <img src="assets/images/samples/avatar-2.html" alt="Post Author Avatar">
                          </figure>
                          <div class="post-author__info">
                            <h4 class="post-author__name">Jessica Hoops</h4>
                          </div>
                        </div>
                        <ul class="post__meta meta">
                          <li class="meta__item meta__item--views">2369</li>
                          <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 530</a></li>
                          <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                        </ul>
                      </footer> #}
                    </div>
                  </div>
                </div>
              {% endfor %}
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar" class="sidebar col-md-4">
        <aside class="widget card widget--sidebar widget-standings">
          <div class="widget__title card__header card__header--has-btn">
            <h4>Recent Esport</h4>
            {# <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">See All League</a> #}
          </div>
          <div class="widget__content card__content">
            <div class="table-responsive">
              <table class="table table-hover table-standings">
                <thead>
                  <tr>
                    <th>League</th>
                    <th class="text-center">Winner</th>
                    {# <th>L</th>
                    <th>GB</th> #}
                  </tr>
                </thead>
                <tbody>
                {% for i, recent_match in home.recent_match %}
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{recent_match.match_image}}" alt="{{recent_match.match_name}}">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">{{recent_match.match_name}}</h6>
                          <span class="team-meta__place">Match : <a href="{{url('matches/' ~ recent_match.match_stats.id)}}">{{recent_match.match_stats.id}}</a></span>
                          <span class="team-meta__place">Date : {{recent_match.match_stats.time_text}}</span>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{recent_match.match_team_win.team_image}}" alt="{{recent_match.match_team_win.team_image}}">
                        </figure>
                        {# <div class="team-meta__info">
                          <h6 class="team-meta__name">{{recent_match.match_team_win.team_name}}</h6>
                        </div> #}
                      </div>
                    </td>
                    {# <td>5</td>
                    <td>0</td> #}
                  </tr>
                {% endfor %}
                </tbody>
              </table>
            </div>
          </div>
        </aside>

        <aside class="widget card widget--sidebar widget-standings">
          <div class="widget__title card__header card__header--has-btn">
            <h4>Recent Hero Win rate</h4>
            {# <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">See All League</a> #}
          </div>
          <div class="widget__content card__content">
            <div class="table-responsive">
              <table class="table table-hover table-standings">
                <thead>
                  <tr>
                    <th>Hero</th>
                    <th class="text-center">Winrate</th>
                  </tr>
                </thead>
                <tbody>
                {% for i, hero in home.recent.hero %}
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{hero.hero_image}}" alt="{{hero.hero_name}}">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">{{hero.hero_name}}</h6>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="team-meta">
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">{{hero.hero_win_rate}}</h6>
                        </div>
                      </div>
                    </td>
                  </tr>
                {% endfor %}
                </tbody>
              </table>
            </div>
          </div>
        </aside>
        {# <aside class="widget widget--sidebar card widget-popular-posts">
          <div class="widget__title card__header">
            <h4>Popular News</h4>
          </div>
          <div class="widget__content card__content">
            <ul class="posts posts--simple-list">
              {% for i, hero in home.recent.hero %}
                <li class="posts__item posts__item--category-1">
                  <figure class="posts__thumb">
                    <a href="#"><img src="{{hero.hero_image}}" alt="{{hero.hero_name}}"></a>
                  </figure>
                  <div class="posts__inner">
                    <div class="posts__cat">
                      <span class="label posts__cat-label">{{hero.hero_name}}</span>
                    </div>
                    <time datetime="2016-08-22" class="posts__date">{{hero.hero_win_rate}}</time>
                  </div>
                </li>
              {% endfor %}
            </ul>
          </div>
        </aside> #}
      </div>
    </div>
  </div>
</div>
{% endblock %}

{% block scripts %}
<script src="{{url('assets/vendor/jquery/jquery.cookie.min.js')}}"></script></script>
<script type="text/javascript">
  $(document).ready(function() {
    if($.cookie('pop')==null){
      $('#notifModal').modal('show');
      $.cookie('pop','1');
    }
  });
</script>
{% endblock %}
