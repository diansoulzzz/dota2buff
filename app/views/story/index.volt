{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-12">
        <div class="card">
          <div class="card__header card__header--has-btn">
            <h4>Dota 2 Story</h4>
          </div>
          <div class="widget__content card__content">
            <ul class="newslog">

              <li class="newslog__item">
                <h4 style="float:left">Dota 2 </h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">
                Dota 2 is an Action RTS game, developed by Valve Corporation. The title was formally announced on October 13, 2010; and was released as a Free to Play game on July 9th, 2013 for Windows, and July 18th, 2013 for Mac OS and Linux. It is the successor to the very popular Warcraft 3 mod, Defense of the Ancients, which was based on the Aeon of Strife map for StarCraft.
                </div>
              </li>


              <li class="newslog__item">
                <h4 style="float:left">Basic Premise</h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">
                  Dota 2 is an RTS-styled competitive team game with RPG elements. Two competing teams (Radiant and Dire) consist of five players each. The main objective in Dota 2 is to destroy the enemy Ancient inside their stronghold. These strongholds are protected by multiple towers down 3 lanes. Instead of building armies of units like in classical RTS games, each player controls a single Hero, a strategically-powerful unit with unique abilities and characteristics which can be improved over the course of the game. Experience is earned when nearby creeps and heroes die, and once collecting enough experience, the hero gains a level, which increases the hero's stats, and at most levels the hero gains a skill point which can be spent to unlock or upgrade one of the hero's abilities. Alongside a hero's fixed abilities, each hero has 6 inventory slots which can be filled with Items which provide various benefits and abilities. To purchase these items, Gold is gained passively over time, by killing creeps, by killing enemy heroes and by destroying buildings.

                  Dota 2 has a heavy emphasis on tactics and team co-ordination, and a deep amount of strategy focused on building up strength as fast as possible, optimal itemization, and selecting what order to upgrade your hero's spells.
                </div>
              </li>

              <li class="newslog__item">
                <h4 style="float:left">Development</h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">

                  IceFrog, the lead developer for the original Defense of the Ancients, was hired by Valve Software in 2009.

                  The beta began in 2010, officially ending in July 2013 just before The International 2013 when the Windows client was officially released to the public with a queue system to deal with high traffic. The Mac and Linux clients were released a week after the general release of the Windows client. Dota 2 runs on the Source 2 Engine and mimics some of the quirks that were present in the original DotA game in order to preserve gameplay.
                </div>
              </li>

              <li class="newslog__item">
                <h4 style="float:left">Map</h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">

                The Map is the playing field for all Dota 2 matches. It is comprised of two sides, one for the Radiant icon.png Radiant faction, and one for the Dire icon.png Dire faction. To win, players must destroy the opposing side's Ancient, an important building at the center of each team's base. The map is represented in the interface by the minimap. It is also important to note that the game only contains one map by default, however, custom games can feature new, community created maps.
                </div>
              </li>


              <li class="newslog__item">
                <h4 style="float:left">Heroes</h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">

                  Heroes are each unique characters within Dota 2. At the start of each game, two teams of five are randomly assigned to Radiant and Dire, then sides take turns selecting a Hero from the Hero Pool. Each player controls a single Hero and any units that hero controls, such as summoned units like Eidolons. The selection of these 10 heroes is commonly referred to as that match's draft.

                  Heroes will gain experience when enemy units are killed nearby. They start the game at level one, and as they earn experience, they eventually level up, and each level grants more increases to the hero's attributes and an ability point to spend on upgrading abilities or more attribute bonuses.

                  In addition to a hero's abilities, each hero has a melee or ranged attack which does an amount of damage based on the primary attribute: strength, agility, or intelligence.

                  Dota 2 has currently implemented 116 heroes. 112 of these heroes were based on heroes from DotA Allstars; and after porting all the DotA heroes, 4 more original heroes have been added.
                </div>
              </li>

              <li class="newslog__item">
                <h4 style="float:left">Items</h4>
                <div style="clear:both;"></div>
                <div class="newslog__content">

                Items are used to give Heroes additional various abilities, attack modifiers, bonus attributes, as well as secondary stats such as armor, attack speed, or improved regeneration of health or mana. Items are sold at Shops, which can be found at the base, the side-lanes, or the more secluded Secret Shops. More powerful items can be constructed by combining weaker items in fixed combinations which require a specific set of items to be in the inventory together. Items take up space in the Hero's inventory, courier's inventory, or stash, all of which have 6 slots for storage and an additional 3 backup space where items are muted and can be switched with the main inventory items with the activation delay of 6 seconds.
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<style>
.newslog__content{
  font-size: 15px;

}
</style>
{% endblock %}
