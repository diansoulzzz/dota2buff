{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-12">
        <div class="card">
          <div class="card__header card__header--has-btn">
            <h3>Dota 2 Events</h3>
          </div>
          <div class="widget__content card__content">
            <ul class="newslog">

              {% for i, event in events %}
                <li class="newslog__item">
                  <h4 style="float:left">{{event.event_title}} - {{event.event_type}}</h4>
                  <img src="{{event.event_image}}" class="pull-right" style="max-height:100px;max-width:200px;"/>
                  <div style="clear:both;"></div>
                  <p class="font-size-15" style="">{{event.event_start}}</p>
                  {% for i, team in event.team %}
                    <div class="newslog__content">

                      <strong> <img src="{{ team.image }}" style="width:50px;" alt=""> {{team.place}} </strong>
                    </div>
                  {% endfor %}
                </li>
              {% endfor %}



            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<style>
.newslog__content{
  font-size: 15px;
}
.font-size-15{
  font-size: 15px;
}

</style>
{% endblock %}
