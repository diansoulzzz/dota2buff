{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="card">
      <div class="card__header card__header--has-btn">
        <h4>Items</h4>
      </div>
      <div class="card__content">
        <div class="table-responsive">
          <table id="dataTables" class="table table-hover team-result">
            <thead>
              <tr>
                <th class="team-standings__pos">#</th>
                <th class="team-result__vs">Item</th>
                <th class="team-result__status">Timed Used</th>
              </tr>
            </thead>
            <tbody>
            {% for i, item in items %}
                <tr>
                  <td>{{i+1}}</td>
                  <td class="team-result__vs">
                    <a href="{{url(item[0][2])}}">
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="{{url('https://www.dotabuff.com' ~ item[0][0])}}" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name" style="color:rgb(48, 178, 251)">{{item[1]}}</h6>
                        </div>
                      </div>
                    </a>
                  </td>
                  <td class="team-result__status">{{item[2]}}</td>
                </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
