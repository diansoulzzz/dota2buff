{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content" style="padding-bottom:0px;">
  <div class="container">
    <div class="products products--list products--list-lg" >
      <div class="product__item product__item--color-1 card">
        <div class="product__img" style="padding:20px;">
          <div class="product__img-holder" style="width:auto">
            <img src="{{url(detail.item_image)}}" style="width:200px;" alt="">
          </div>
        </div>
        <div class="product__content card__content" style="padding:30px">
          <header class="product__header" style="margin-bottom:0px;">
            <div class="product__header-inner">
              <h2 class="product__title">{{detail.item_name}}</h2>
              {% for i, shop in detail.item_shop %}
              <div style="clear:both;">
                <img src="{{url(shop.icon)}}" style="width:50px;float:left;" alt="">
                <h4 style="font-size: 14px;float:left;margin-top:20px;">Available on {{shop.name}}</h4>
              </div>
              {% endfor %}
            </div>
          </header>
        </div>
        <span class="product__category" style="position:absolute;right:15%;margin-top:20px">
          <div style="clear:both;" >
            <img src="{{detail.item_price.icon}}" style="width:50px;float:left;"/>
            <h4 style="float:left;margin-top:20px;">{{detail.item_price.value}}</h4>
          </div>
        </span>
      </div>
    </div>
    {# <div class="product-tabs card card--xlg">
      <ul class="nav nav-tabs nav-justified nav-product-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab-abilities" role="tab" data-toggle="tab"><small>Heroes</small>Abilities</a></li>
      </ul>
    </div> #}
  </div>
</div>
<div class="tab-content card__content">
  <div role="tabpanel" class="tab-pane fade in active" id="tab-abilities">
    <div class="site-content" style="padding-top:0px;">
      <div class="container">
        <div class="row">
          <div class="content col-md-6">
            <div class="card">
              <div class="card__header card__header--has-btn">
                <h4>Item Abilities</h4>
              </div>
              <div class="widget__content card__content">
                <ul class="newslog">
                  <li class="newslog__item" style="padding-bottom:0px;">
                    <h5>Skill</h5>
                    {% for i, desc in detail.item_description %}
                      {{desc}}
                      <hr />
                    {% endfor %}
                  </li>
                  {% if detail.item_stats|length > 0 %}
                  <li class="newslog__item">
                    <h5>Attribute</h5>
                    {% for i, stats in detail.item_stats %}
                      +{{stats.value}} {{stats.ability}}
                      <br />
                    {% endfor %}
                  </li>
                  {% endif %}
                </ul>
              </div>
            </div>
          </div>

          {% if detail.item_builds.from|length > 0 or detail.item_builds.into|length > 0 %}
          <div class="content col-md-6">
            <div class="card">
              <div class="card__header card__header--has-btn">
                <h4>Item Builds</h4>
              </div>
              <div class="widget__content card__content">
                <ul class="newslog">
                  {% if detail.item_builds.from|length > 0 %}
                  <li class="newslog__item" style="padding-bottom:0px;">
                    <h5>Build From</h5>
                    {% for i, build in detail.item_builds.from %}
                      <a href="{{url(build.link)}}" style="padding-right:5px; float:left">
                        <img src="{{build.image}}" style="width:50px;height:50px"/>
                        <p class="text-center">{{build.cost}}</p>
                      </a>
                    {% endfor %}
                    <div style="clear:both"></div>
                  </li>
                  {% endif %}
                  {% if detail.item_builds.into|length > 0 %}
                  <li class="newslog__item" style="padding-bottom:0px;">
                    <h5>Build Into</h5>
                    {% for i, build in detail.item_builds.into %}
                      <a href="{{url(build.link)}}" style="padding-right:5px; float:left">
                        <img src="{{build.image}}" style="width:50px;height:50px"/>
                        <p class="text-center">{{build.cost}}</p>
                      </a>
                    {% endfor %}
                    <div style="clear:both"></div>
                  </li>
                  {% endif %}
                </ul>
              </div>
            </div>
          </div>
          {% endif %}
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
