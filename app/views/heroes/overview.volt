{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content" style="padding-bottom:0px;">
  <div class="container">
    <div class="products products--list products--list-lg" >
      <div class="product__item product__item--color-1 card">
        <div class="product__img">
          <div class="product__img-holder">
            <div class="product__slider">
              <div class="product__slide">
                <div class="product__slide-img" >
                  <img src="{{url('https://api.opendota.com' ~ hero.info.img)}}" style="width:700px;" alt="">
                   {# height: 500px; #}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="product__content card__content">
          <header class="product__header">
            <div class="product__header-inner">
              <span class="product__category">
                <span style="color:white">{{hero.info.attack_type}}</span>
                {% for i, role in hero.info.roles %}
                  <span>, {{role}}</span>
                {% endfor %}
              </span>
              <h2 class="product__title">{{hero.info.localized_name}}</h2>
            </div>
            <div class="product__price">
              {% if hero.info.primary_attr === 'agi' %}
                <svg style="width: 20px; margin-right: 4px;" viewBox="0 0 512 512"><path fill="#39d401" d="M164.672 15.316c-4.24-.02-8.52-.008-12.848.032 356.973 34.267 149.668 296.606-133.02 225.675v29.272c208.715 52.028 406.9-83.077 335.225-186.316 74.252 54.5 10.927 228.767-217.44 261.272 80.052-17.795 151.75-58.013 188.793-112.78v-.003c-76.777 75.27-199.896 99.73-306.61 83.514v38.547l.03.003v29.983c103.604 17.95 230.47-10.83 317.05-98.192-64.335 91.95-198.984 149.52-317.05 142.64v62.942C398.408 491.783 590.073 234.433 449.346 98c90.898 155.644-119.865 338.862-308.12 339.258C392.92 399.278 523.24 116.29 322.532 33.352c-43.685-11.26-96.104-17.76-157.86-18.036z"></path></svg>
              {% elseif hero.info.primary_attr === 'str' %}
                <svg viewBox="0 0 512 512" style="width: 20px; margin-right: 4px;"><path fill="#f44336" d="M329.8 235.69l62.83-82.71 42.86 32.56-62.83 82.75zm-12.86-9.53l66.81-88-45-34.15-66.81 88zm-27.48-97.78l-19.3 39.57 57-75-42.51-32.3-36.24 47.71zm-20.74-73.24l-46.64-35.43-42 55.31 53.67 26.17zm107 235.52l-139-102.71-9.92.91 4.56 2 62.16 138.43-16.52 2.25-57.68-128.5-40-17.7-4-30.84 39.41 19.42 36.36-3.33 17-34.83-110.9-54.09-80.68 112.51L177.6 346.67l-22.7 145.62H341V372.62l35.29-48.93L387 275.77z"></path></svg>
              {% else %}
                <svg viewBox="0 0 512 512" style="width: 20px; margin-right: 4px;"><path fill="#03a9f4" d="M335.656 19.53c-24.51.093-48.993 5.235-71.062 15.626-22.46 10.577-43.112 34.202-58.375 62.563-15.264 28.36-25.182 61.262-27.69 88.75-7.487 82.112-51.926 155.352-159.78 252.56l-.188 21.44C89.216 403.443 139.915 346.632 176.313 290l.063.03c-9.293 32.473-22.623 63.18-43.594 87.97-31.47 35.584-69.222 71.1-114.468 106.53l-.062 8.25 25 .064h.47l1.28-1.156c24.405-16.498 48.607-31.488 72.594-41.5l.187.187-46.436 42.5 28.937.063c48.372-41.685 94.714-90.58 129.626-137 33.587-44.658 56.02-87.312 60.688-116.844-1.268-2.32-2.552-4.628-3.656-7.094-18.833-42.06-4.273-96.424 40.218-116.063 32.73-14.45 74.854-3.165 90.438 31.344.15.333.324.634.47.97 13.302 24.062 6.175 49.48-9.345 61.97-7.866 6.328-18.442 9.528-28.75 6.56-10.31-2.966-19.043-11.772-24.5-25.124l17.28-7.062c3.992 9.764 8.667 13.15 12.375 14.22 3.708 1.066 7.767.148 11.875-3.158 8.216-6.61 14.282-21.91 4.406-39.03l-.28-.47-.22-.5c-10.7-24.82-41.96-33.333-66.22-22.625-34.063 15.037-45.594 58.052-30.686 91.345 20.527 45.846 77.97 61.177 122.375 40.875 60.157-27.5 80.13-103.328 53.094-161.813-24.737-53.503-81.41-82.484-138.908-83.843-1.633-.04-3.272-.07-4.906-.063zm-25.75 26.72c3.238.035 6.363.348 9.406.906 10.343 1.898 19.946 6.753 29.032 13.25-30.623-5.437-58.324 4.612-80.78 24.782-22.44 20.152-39.16 50.59-45.783 84.718-4.655-11.358-7.166-21.462-6.686-31.72.296-6.343 1.715-12.956 4.78-20.217 9.094-18.016 21.032-33.946 35.22-46.69 7.824-7.026 16.39-13.07 25.53-17.905 10.932-5.212 20.522-7.22 29.282-7.125zm122.938 62.313c22.583 13.167 34.365 41.86 32.937 70.656-.564 11.395-3.466 22.975-8.905 33.624-12.48 18.937-35.53 25.51-49.97 20.875l-.092-.25c27.943-10.365 39.18-32.377 40.312-55.19.124-2.5.115-4.994-.03-7.468 1.447-13.31-.412-28.793-5.47-43.437-2.244-6.496-5.15-12.89-8.844-18.72l.064-.093zm-135.563 1.312c-20.97 19.342-29.406 35.252-33.25 51.25-3.848 16.023-2.788 32.84-2.905 52.875-.14 23.79-2.56 51.542-18.438 85.688-.005.012-.025.018-.03.03-21.095 26.753-45.276 52.25-68.907 67.376l-.063-.03c64.195-71.545 68.527-114.792 68.75-153.19.112-19.197-1.253-37.594 3.438-57.124.57-2.37 1.233-4.742 2-7.125h.03c8.098-17.036 16.572-26.058 25.47-31.563 7.18-4.44 15.035-6.697 23.906-8.187z"></path></svg>
              {% endif %}
              {{hero.info.primary_attr}}
            </div>
          </header>

          <div class="product__excerpt">
            <h5>Base Attribute</h5>
            <h6 style="float:left;margin-right:10px;">
              <svg style="width: 20px; margin-right: 4px;" viewBox="0 0 512 512"><path fill="#39d401" d="M164.672 15.316c-4.24-.02-8.52-.008-12.848.032 356.973 34.267 149.668 296.606-133.02 225.675v29.272c208.715 52.028 406.9-83.077 335.225-186.316 74.252 54.5 10.927 228.767-217.44 261.272 80.052-17.795 151.75-58.013 188.793-112.78v-.003c-76.777 75.27-199.896 99.73-306.61 83.514v38.547l.03.003v29.983c103.604 17.95 230.47-10.83 317.05-98.192-64.335 91.95-198.984 149.52-317.05 142.64v62.942C398.408 491.783 590.073 234.433 449.346 98c90.898 155.644-119.865 338.862-308.12 339.258C392.92 399.278 523.24 116.29 322.532 33.352c-43.685-11.26-96.104-17.76-157.86-18.036z"></path></svg>
              {{hero.info.base_str}} + {{hero.info.str_gain}}
            </h6>
            <h6 style="float:left;margin-right:10px;">
              <svg viewBox="0 0 512 512" style="width: 20px; margin-right: 4px;"><path fill="#f44336" d="M329.8 235.69l62.83-82.71 42.86 32.56-62.83 82.75zm-12.86-9.53l66.81-88-45-34.15-66.81 88zm-27.48-97.78l-19.3 39.57 57-75-42.51-32.3-36.24 47.71zm-20.74-73.24l-46.64-35.43-42 55.31 53.67 26.17zm107 235.52l-139-102.71-9.92.91 4.56 2 62.16 138.43-16.52 2.25-57.68-128.5-40-17.7-4-30.84 39.41 19.42 36.36-3.33 17-34.83-110.9-54.09-80.68 112.51L177.6 346.67l-22.7 145.62H341V372.62l35.29-48.93L387 275.77z"></path></svg>
              {{hero.info.base_agi}} + {{hero.info.agi_gain}}
            </h6>
            <h6>
              <svg viewBox="0 0 512 512" style="width: 20px; margin-right: 4px;"><path fill="#03a9f4" d="M335.656 19.53c-24.51.093-48.993 5.235-71.062 15.626-22.46 10.577-43.112 34.202-58.375 62.563-15.264 28.36-25.182 61.262-27.69 88.75-7.487 82.112-51.926 155.352-159.78 252.56l-.188 21.44C89.216 403.443 139.915 346.632 176.313 290l.063.03c-9.293 32.473-22.623 63.18-43.594 87.97-31.47 35.584-69.222 71.1-114.468 106.53l-.062 8.25 25 .064h.47l1.28-1.156c24.405-16.498 48.607-31.488 72.594-41.5l.187.187-46.436 42.5 28.937.063c48.372-41.685 94.714-90.58 129.626-137 33.587-44.658 56.02-87.312 60.688-116.844-1.268-2.32-2.552-4.628-3.656-7.094-18.833-42.06-4.273-96.424 40.218-116.063 32.73-14.45 74.854-3.165 90.438 31.344.15.333.324.634.47.97 13.302 24.062 6.175 49.48-9.345 61.97-7.866 6.328-18.442 9.528-28.75 6.56-10.31-2.966-19.043-11.772-24.5-25.124l17.28-7.062c3.992 9.764 8.667 13.15 12.375 14.22 3.708 1.066 7.767.148 11.875-3.158 8.216-6.61 14.282-21.91 4.406-39.03l-.28-.47-.22-.5c-10.7-24.82-41.96-33.333-66.22-22.625-34.063 15.037-45.594 58.052-30.686 91.345 20.527 45.846 77.97 61.177 122.375 40.875 60.157-27.5 80.13-103.328 53.094-161.813-24.737-53.503-81.41-82.484-138.908-83.843-1.633-.04-3.272-.07-4.906-.063zm-25.75 26.72c3.238.035 6.363.348 9.406.906 10.343 1.898 19.946 6.753 29.032 13.25-30.623-5.437-58.324 4.612-80.78 24.782-22.44 20.152-39.16 50.59-45.783 84.718-4.655-11.358-7.166-21.462-6.686-31.72.296-6.343 1.715-12.956 4.78-20.217 9.094-18.016 21.032-33.946 35.22-46.69 7.824-7.026 16.39-13.07 25.53-17.905 10.932-5.212 20.522-7.22 29.282-7.125zm122.938 62.313c22.583 13.167 34.365 41.86 32.937 70.656-.564 11.395-3.466 22.975-8.905 33.624-12.48 18.937-35.53 25.51-49.97 20.875l-.092-.25c27.943-10.365 39.18-32.377 40.312-55.19.124-2.5.115-4.994-.03-7.468 1.447-13.31-.412-28.793-5.47-43.437-2.244-6.496-5.15-12.89-8.844-18.72l.064-.093zm-135.563 1.312c-20.97 19.342-29.406 35.252-33.25 51.25-3.848 16.023-2.788 32.84-2.905 52.875-.14 23.79-2.56 51.542-18.438 85.688-.005.012-.025.018-.03.03-21.095 26.753-45.276 52.25-68.907 67.376l-.063-.03c64.195-71.545 68.527-114.792 68.75-153.19.112-19.197-1.253-37.594 3.438-57.124.57-2.37 1.233-4.742 2-7.125h.03c8.098-17.036 16.572-26.058 25.47-31.563 7.18-4.44 15.035-6.697 23.906-8.187z"></path></svg>
              {{hero.info.base_int}} + {{hero.info.int_gain}}
            </h6>
          </div>

          <div class="product__excerpt">
            <h5>Base Status</h5>
            <div class="row">
              <div class="col-sm-4">
                <h6>Base attack: {{hero.info.base_attack_min}} - {{hero.info.base_attack_max}} </h6>
              </div>
              <div class="col-sm-4">
                <h6>Attack range: {{hero.info.attack_range}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Attack speed: {{hero.info.attack_rate}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Health: {{hero.info.base_health}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Health regen: {{hero.info.base_health_regen}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Mana: {{hero.info.base_mana}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Mana regen: {{hero.info.base_mana_regen}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Move speed: {{hero.info.move_speed}}</h6>
              </div>
              <div class="col-sm-4">
                <h6>Turn speed: {{hero.info.turn_rate}}</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="product-tabs card card--xlg">
      <ul class="nav nav-tabs nav-justified nav-product-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab-abilities" role="tab" data-toggle="tab"><small>Heroes</small>Abilities</a></li>
        <li role="presentation"><a href="#tab-matches" role="tab" data-toggle="tab"><small>Recent</small>League</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="tab-content card__content">
  <div role="tabpanel" class="tab-pane fade" id="tab-matches">
    <div class="site-content" style="padding-top:0px;">
      <div class="container">
        <div class="row">
          <div class="content col-md-12">
            <div class="card">
              <div class="card__header card__header--has-btn">
                <h4>Heroes Played</h4>
              </div>
              <div class="card__content">
                <div class="table-responsive">
                  <table id="dataTables" class="table table-hover team-result">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th class="team-result__status">League</th>
                        <th class="team-result__vs">Player ID</th>
                        <th>Result</th>
                      </tr>
                    </thead>
                    <tbody>
                    {% for i, matches in hero.matches %}
                        <tr>
                          <td>{{i+1}}</td>
                          <td class="team-result__status">{{matches.league_name}}</td>
                          <td class="team-result__vs">
                            <div class="team-meta">
                              <figure class="team-meta__logo">
                                <img src="" alt="">
                              </figure>
                              <div class="team-meta__info">
                                <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url('players/'~matches.account_id)}}">{{matches.account_id}}</a></h6>
                              </div>
                            </div>
                          </td>
                          <td style="{{matches.radiant == matches.radiant_win?'color:green':'color:red'}}">{{matches.radiant == matches.radiant_win ?'Win':'Lose'}}</td>
                        </tr>
                    {% endfor %}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div role="tabpanel" class="tab-pane fade in active" id="tab-abilities">
    <div class="site-content" style="padding-top:0px;">
      <div class="container">
        <div class="row">
          <div class="content col-md-8">
            <div class="card">
              <div class="card__header card__header--has-btn">
                <h4>Hero Abilities</h4>
              </div>
              <div class="widget__content card__content">
                <ul class="newslog">
                  {% for i, ability in hero.skill.abilities %}
                    <li class="newslog__item">
                      <h4 style="float:left">{{ability.ability_name}}</h4>
                      <img src="{{ability.ability_image}}" class="pull-right" style="max-height:50px;max-width:50px;"/>
                      <div style="clear:both;"></div>
                      {# <hr /> #}
                      {% for i, detail in ability.ability_detail %}
                        <div class="newslog__content">
                          <strong>{{detail.name}} : </strong> {{detail.value}}.
                        </div>
                      {% endfor %}
                      <hr />
                      <div class="newslog__content">
                        {{ability.ability_desc}}
                      </div>
                      {% if ability.ability_cooldown|length > 0 %}
                      <div>
                        <hr />
                        <div class="newslog__content" style="float:left">
                          <strong>Cooldown :</strong>
                          {% for i, cooldown in ability.ability_cooldown %}
                            <strong>{{cooldown}}</strong>
                            {% if i < loop.length-1 %} / {% endif %}
                          {% endfor %}
                        </div>
                        <div class="newslog__content pull-right">
                          <strong>Manacost :</strong>
                          {% for i, manacost in ability.ability_manacost %}
                            <strong>{{manacost}}</strong>
                            {% if i < loop.length-1 %} / {% endif %}
                          {% endfor %}
                        </div>
                      </div>
                      {% endif %}

                      <div style="clear:both;"></div>
                      <hr />
                      {% for i, stats in ability.ability_stats %}
                        <div class="newslog__content">
                          <strong>{{stats.name}} : </strong> {{stats.value}}.
                        </div>
                      {% endfor %}
                      <div style="clear:both;"></div>
                    </li>
                  {% endfor %}
                </ul>
              </div>
            </div>
          </div>

          <div class="sidebar col-md-4">
            <aside class="widget card widget--sidebar widget-standings">
              <div class="widget__title card__header card__header--has-btn">
                <h4>Hero Talent</h4>
              </div>
              <div class="widget-player__details">
                <div class="widget-player__details-row">
                  <div class="widget-player__details__item">
                    <div class="widget-player__details-desc-wrapper">
                      <span class="widget-player__details-holder">
                        <span class="widget-player__details-label text-center"><b>Tree Left</b></span>
                      </span>
                    </div>
                  </div>
                  <div class="widget-player__details__item">
                    <div class="widget-player__details-desc-wrapper">
                      <span class="widget-player__details-holder">
                        <span class="widget-player__details-label text-center"><b>Tree Right</b></span>
                      </span>
                    </div>
                  </div>
                  <div class="widget-player__details__item">
                    <div class="widget-player__details-desc-wrapper">
                      <span class="widget-player__details-holder">
                        <span class="widget-player__details-label text-center">><b>Level</b></span>
                      </span>
                    </div>
                  </div>
                </div>
                {% for i, talent in hero.skill.talent %}
                  <div class="widget-player__details-row">
                    <div class="widget-player__details__item">
                      <div class="widget-player__details-desc-wrapper">
                        <span class="widget-player__details-holder">
                          <span class="widget-player__details-label">{{talent.left}}</span>
                        </span>
                      </div>
                    </div>
                    <div class="widget-player__details__item">
                      <div class="widget-player__details-desc-wrapper">
                        <span class="widget-player__details-holder">
                          <span class="widget-player__details-label">{{talent.right}}</span>
                        </span>
                      </div>
                    </div>
                    <div class="widget-player__details__item">
                      <div class="widget-player__details-desc-wrapper">
                        <span class="widget-player__details-holder">
                          <span class="widget-player__details-label">{{talent.level}}</span>
                        </span>
                      </div>
                    </div>
                  </div>
                {% endfor %}
              </div>


            </aside>
            {# <aside class="widget card widget--sidebar widget-standings">
              <div class="widget__title card__header card__header--has-btn">
                <h4>Heroes Played</h4>
              </div>
              <div class="widget__content card__content">

              </div>
            </aside> #}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
