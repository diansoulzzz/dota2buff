{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="card">
      <div class="card__header card__header--has-btn">
        <h4>Heroes</h4>
      </div>
      <div class="card__content">
        <div class="table-responsive">
          <table id="dataTables" class="table table-hover team-result">
            <thead>
              <tr>
                <th class="team-standings__pos">#</th>
                <th class="team-result__vs">Hero</th>
                <th class="team-result__status">Primary Attribute</th>
                <th class="team-result__status">Attack Type</th>
                <th class="team-result__status">Roles</th>
              </tr>
            </thead>
            <tbody>
            {% for i, hero in heroes %}
                <tr>
                  <td>{{i+1}}</td>
                  <td class="team-result__vs">
                    <div class="team-meta">
                      <figure class="team-meta__logo">
                        <img src="{{url('https://api.opendota.com' ~ hero.img)}}" alt="">
                      </figure>
                      <div class="team-meta__info">
                        <h6 class="team-meta__name"><a style="color:rgb(102, 187, 255)" href="{{url('heroes/'~hero.id)}}">{{hero.localized_name}}</a></h6>
                      </div>
                    </div>
                  </td>
                  <td class="team-result__status">{{hero.primary_attr}}</td>
                  <td class="team-result__status">{{hero.attack_type}}</td>
                  <td class="team-result__status">
                      {% for i, role in hero.roles %}
                        <span>{{role}}</span>
                      {% endfor %}
                  </td>
                </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
};
</script>
{% endblock %}
