{% extends 'layouts/default.volt' %}

{% block content %}
<div class="site-content">
  <div class="container">
    <div class="card">
      <div class="card__header card__header--has-btn">
        <h4>Heroes</h4>
      </div>
      <div class="card__content">
        <form method="get" id="form-filter" action="{{url('heroes/trends')}}">
          <div class="form-group">
            <label for="select-filter">MOST PLAYED: {{s_filter}}</label>
            <select class="form-control" id="select-filter" name="filter">
              {% for i, option in trends.filters %}
                <option value="{{option.value}}" {{option.disabled}} style="color:black" {% if s_filter == option.value %} selected {% endif %}>
                  {{option.text}}
                </option>
              {% endfor %}
            </select>
          </div>
        </form>
        <div class="table-responsive">
          <table id="dataTables" class="table table-hover team-result">
            <thead>
              <tr>
                <th class="team-standings__pos">#</th>
                <th class="team-result__vs">Hero</th>
                <th class="team-result__status">Matches Played</th>
                <th class="team-result__status">Pick Rate</th>
                <th class="team-result__status">Win Rate</th>
                <th class="team-result__status">KDA Ratio</th>
              </tr>
            </thead>
            <tbody>
            {% for i, hero in trends.heroes %}
                <tr>
                  <td>{{i+1}}</td>
                  <td class="team-result__vs">
                    <div class="team-meta">
                      <figure class="team-meta__logo">
                        <img src="{{hero.image}}" alt="">
                      </figure>
                      <div class="team-meta__info">
                        <h6 class="team-meta__name"><a href="javascript:void(0);">{{hero.name}}</a></h6>
                      </div>
                    </div>
                  </td>
                  <td class="team-result__status">{{hero.match.played}}</td>
                  <td class="team-result__status">
                    {{hero.match.pick_rate}}%
                    <div class="bar-container">
                      <div class="percent red" style="width:{{hero.match.pick_rate}}%;"></div>
                    </div>
                  </td>
                  <td class="team-result__status">
                    {{hero.match.win_rate}}%
                    <div class="bar-container">
                      <div class="percent blue" style="width:{{hero.match.win_rate}}%;"></div>
                    </div>
                  </td>
                  <td class="team-result__status">
                    {{hero.match.kda_ratio}}
                  </td>
                </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
{% block styles %}
<link href="{{url('assets/vendor/dataTables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/dataTables/custom.dataTables.css')}}" rel="stylesheet">

<style>
.mvp {
  border-radius: 25%;
  color:black;
  line-height: 30px;
  font-weight: bold;
  text-align: center;
  height: 30px;
  width: 70px;
  margin: auto;
}
.kill {
  background: rgba(255, 215, 0, 0.59);
  font-weight: bold;
  color:black;
}
.feed {
  background: rgba(255, 0, 0, 0.64);
}
.assist {
  background: rgba(0, 79, 255, 0.44);
}
.mvp.kill {
  background: rgb(255,215,0);
}
.mvp.feed {
  background: rgb(255, 0, 0);
}
.mvp.assist {
  background: rgb(0, 79, 255);
}
.bar-container {
  width: 100%;
  background-color: rgb(150, 150, 150);
  height: auto;
  margin: auto;
}
.bar-container .percent.green {
  background-color: rgb(50, 231, 5);
  height: 10px;
}
.bar-container .percent.blue {
  background: rgb(0, 79, 255);
  height: 10px;
}
.bar-container .percent.red {
  background-color: rgb(255, 0, 0);
  height: 10px;
}
.bar-container .percent.yellow {
  background-color: rgb(255,215,0);
  height: 10px;
}

.team-radiant {
  color: rgb(76, 237, 83) !important;
  fill: currentColor;
}
.team-radiant h5 {
  color: currentColor;
}
.team-win {
  filter: drop-shadow(rgb(255, 171, 64) 0px 0px 4px);
}
.team-dire {
  color: rgb(221, 38, 13) !important;
  fill: currentColor;
}
.team-dire h5 {
  color: currentColor;
}
.score-lose {
  color: rgb(139, 139, 139);
}
.score-win {
  color: rgb(242, 234, 33);
}
</style>
{% endblock %}
{% block scripts %}
<script src="{{url('assets/vendor/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendor/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.table_init();
    thisform.select_filter();
  },
  table_init: function ()
  {
    $('#dataTables').DataTable({
      "pagingType": "numbers",
    });
  },
  select_filter : function() {
    $('#select-filter').on('change',function (){
      $('#form-filter').submit();
    })
  },
};
</script>
{% endblock %}
