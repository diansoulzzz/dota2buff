{% extends 'layouts/default.volt' %}

{% block content %}
<div class="hero-unit">
  <div class="container hero-unit__container">
    <div class="hero-unit__content hero-unit__content--left-center">
      <span class="hero-unit__decor">
        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
      </span>
      <h5 class="hero-unit__subtitle">Elric Bros School</h5>
      <h1 class="hero-unit__title">The <span class="text-primary">Dota2Buff</span></h1>
      <div class="hero-unit__desc">Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore .</div>
      <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed hero-unit__btn">Read More <i class="fa fa-plus text-primary"></i></a>
    </div>
    <figure class="hero-unit__img">
      <img src="assets/images/samples/header_player.html" alt="Hero Unit Image">
    </figure>
  </div>
</div>
{# <div class="posts posts--carousel-featured featured-carousel">
  <div class="posts__item posts__item--category-1">
    <a href="#" class="posts__link-wrapper">
      <figure class="posts__thumb">
        <img src="assets/images/samples/featured-carousel-2.html" alt="">
      </figure>
      <div class="posts__inner">
        <div class="posts__cat">
          <span class="label posts__cat-label">The Team</span>
        </div>
        <h3 class="posts__title">Dota2Buff women team tryouts will start in January</h3>
        <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
        <ul class="post__meta meta">
          <li class="meta__item meta__item--views">2369</li>
          <li class="meta__item meta__item--likes"><i class="meta-like icon-heart"></i> 530</li>
          <li class="meta__item meta__item--comments">18</li>
        </ul>
      </div>
    </a>
  </div>
  <div class="posts__item posts__item--category-1">
    <a href="#" class="posts__link-wrapper">
      <figure class="posts__thumb">
        <img src="assets/images/samples/featured-carousel-3.html" alt="">
      </figure>
      <div class="posts__inner">
        <div class="posts__cat">
          <span class="label posts__cat-label">The Team</span>
        </div>
        <h3 class="posts__title">Checkout the new ride of our best player of the season</h3>
        <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
        <ul class="post__meta meta">
          <li class="meta__item meta__item--views">2369</li>
          <li class="meta__item meta__item--likes"><i class="meta-like icon-heart"></i> 530</li>
          <li class="meta__item meta__item--comments">18</li>
        </ul>
      </div>
    </a>
  </div>
  <div class="posts__item posts__item--category-1">
    <a href="#" class="posts__link-wrapper">
      <figure class="posts__thumb">
        <img src="assets/images/samples/featured-carousel-1.html" alt="">
      </figure>
      <div class="posts__inner">
        <div class="posts__cat">
          <span class="label posts__cat-label">The Team</span>
        </div>
        <h3 class="posts__title">All the players are taking a team trip this summer</h3>
        <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
        <ul class="post__meta meta">
          <li class="meta__item meta__item--views">2369</li>
          <li class="meta__item meta__item--likes"><i class="meta-like icon-heart"></i> 530</li>
          <li class="meta__item meta__item--comments">18</li>
        </ul>
      </div>
    </a>
  </div>
  <div class="posts__item posts__item--category-1">
    <a href="#" class="posts__link-wrapper">
      <figure class="posts__thumb">
        <img src="assets/images/samples/featured-carousel-2.html" alt="">
      </figure>
      <div class="posts__inner">
        <div class="posts__cat">
          <span class="label posts__cat-label">The Team</span>
        </div>
        <h3 class="posts__title">Dota2Buff women team tryouts will start in January</h3>
        <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
        <ul class="post__meta meta">
          <li class="meta__item meta__item--views">2369</li>
          <li class="meta__item meta__item--likes"><i class="meta-like icon-heart"></i> 530</li>
          <li class="meta__item meta__item--comments">18</li>
        </ul>
      </div>
    </a>
  </div>
  <div class="posts__item posts__item--category-1">
    <a href="#" class="posts__link-wrapper">
      <figure class="posts__thumb">
        <img src="assets/images/samples/featured-carousel-1.html" alt="">
      </figure>
      <div class="posts__inner">
        <div class="posts__cat">
          <span class="label posts__cat-label">The Team</span>
        </div>
        <h3 class="posts__title">All the players are taking a team trip this summer</h3>
        <time datetime="2017-08-23" class="posts__date">August 23rd, 2017</time>
        <ul class="post__meta meta">
          <li class="meta__item meta__item--views">2369</li>
          <li class="meta__item meta__item--likes"><i class="meta-like icon-heart"></i> 530</li>
          <li class="meta__item meta__item--comments">18</li>
        </ul>
      </div>
    </a>
  </div>
</div> #}
<div class="site-content">
  <div class="container">
    <div class="row">
      <div class="content col-md-8">
        <div class="card card--clean">
          <header class="card__header card__header--has-btn">
            <h4>Latest News</h4>
            <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">See All Posts</a>
          </header>
          <div class="card__content">
            <div class="posts posts--cards posts--cards-thumb-left post-list">
              <div class="post-list__item">
                <div class="posts__item posts__item--card posts__item--category-1 card">
                  <figure class="posts__thumb">
                    <a href="#"><img src="assets/images/samples/post-img9-m.html" alt=""></a>
                    <a href="#" class="posts__cta"></a>
                  </figure>
                  <div class="posts__inner">
                    <div class="card__content">
                      <div class="posts__cat">
                        <span class="label posts__cat-label">The Team</span>
                      </div>
                      <h6 class="posts__title"><a href="#">The team is taking a summer vacation on Woody Valley</a></h6>
                      <time datetime="2016-08-17" class="posts__date">August 17th, 2016</time>
                      <div class="posts__excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </div>
                    </div>
                    <footer class="posts__footer card__footer">
                      <div class="post-author">
                        <figure class="post-author__avatar">
                          <img src="assets/images/samples/avatar-2.html" alt="Post Author Avatar">
                        </figure>
                        <div class="post-author__info">
                          <h4 class="post-author__name">Jessica Hoops</h4>
                        </div>
                      </div>
                      <ul class="post__meta meta">
                        <li class="meta__item meta__item--views">2369</li>
                        <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 530</a></li>
                        <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                      </ul>
                    </footer>
                  </div>
                </div>
              </div>
              <div class="post-list__item">
                <div class="posts__item posts__item--card posts__item--category-1 card">
                  <figure class="posts__thumb">
                    <a href="#"><img src="assets/images/samples/post-img10-m.html" alt=""></a>
                    <a href="#" class="posts__cta"></a>
                  </figure>
                  <div class="posts__inner">
                    <div class="card__content">
                      <div class="posts__cat">
                        <span class="label posts__cat-label">The Team</span>
                      </div>
                      <h6 class="posts__title"><a href="#">Jeremy Rittersen was called to be in the National Team</a></h6>
                      <time datetime="2016-08-16" class="posts__date">August 16th, 2016</time>
                      <div class="posts__excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </div>
                    </div>
                    <footer class="posts__footer card__footer">
                      <div class="post-author">
                        <figure class="post-author__avatar">
                          <img src="assets/images/samples/avatar-1.html" alt="Post Author Avatar">
                        </figure>
                        <div class="post-author__info">
                          <h4 class="post-author__name">James Spiegel</h4>
                        </div>
                      </div>
                      <ul class="post__meta meta">
                        <li class="meta__item meta__item--views">2369</li>
                        <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 530</a></li>
                        <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                      </ul>
                    </footer>
                  </div>
                </div>
              </div>
              <div class="post-list__item">
                <div class="posts__item posts__item--card posts__item--category-1 card">
                  <figure class="posts__thumb">
                    <a href="#"><img src="assets/images/samples/post-img11-m.html" alt=""></a>
                    <a href="#" class="posts__cta"></a>
                  </figure>
                  <div class="posts__inner">
                    <div class="card__content">
                      <div class="posts__cat">
                        <span class="label posts__cat-label">The Team</span>
                      </div>
                      <h6 class="posts__title"><a href="#">Dota2Buff New Stadium is gonna be ready in September 2017</a></h6>
                      <time datetime="2016-08-15" class="posts__date">August 15th, 2016</time>
                      <div class="posts__excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </div>
                    </div>
                    <footer class="posts__footer card__footer">
                      <div class="post-author">
                        <figure class="post-author__avatar">
                          <img src="assets/images/samples/avatar-2.html" alt="Post Author Avatar">
                        </figure>
                        <div class="post-author__info">
                          <h4 class="post-author__name">Jessica Hoops</h4>
                        </div>
                      </div>
                      <ul class="post__meta meta">
                        <li class="meta__item meta__item--views">2369</li>
                        <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 530</a></li>
                        <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                      </ul>
                    </footer>
                  </div>
                </div>
              </div>
              <div class="post-list__item">
                <div class="posts__item posts__item--card posts__item--category-1 card">
                  <figure class="posts__thumb">
                    <a href="#"><img src="assets/images/samples/post-img14-m.html" alt=""></a>
                    <a href="#" class="posts__cta"></a>
                  </figure>
                  <div class="posts__inner">
                    <div class="card__content">
                      <div class="posts__cat">
                        <span class="label posts__cat-label">The Team</span>
                      </div>
                      <h6 class="posts__title"><a href="#">The Championship Final will be be played in San Francisco</a></h6>
                      <time datetime="2016-08-18" class="posts__date">August 18th, 2016</time>
                      <div class="posts__excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </div>
                    </div>
                    <footer class="posts__footer card__footer">
                      <div class="post-author">
                        <figure class="post-author__avatar">
                          <img src="assets/images/samples/avatar-2.html" alt="Post Author Avatar">
                        </figure>
                        <div class="post-author__info">
                          <h4 class="post-author__name">Jessica Hoops</h4>
                        </div>
                      </div>
                      <ul class="post__meta meta">
                        <li class="meta__item meta__item--views">2369</li>
                        <li class="meta__item meta__item--likes"><a href="#"><i class="meta-like icon-heart"></i> 530</a></li>
                        <li class="meta__item meta__item--comments"><a href="#">18</a></li>
                      </ul>
                    </footer>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar" class="sidebar col-md-4">
        <aside class="widget card widget--sidebar widget-standings">
          <div class="widget__title card__header card__header--has-btn">
            <h4>Playoff Standings</h4>
            <a href="#" class="btn btn-default btn-outline btn-xs card-header__button">See All Stats</a>
          </div>
          <div class="widget__content card__content">
            <div class="table-responsive">
              <table class="table table-hover table-standings">
                <thead>
                  <tr>
                    <th>Team Positions</th>
                    <th>W</th>
                    <th>L</th>
                    <th>GB</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/pirates_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">L.A Pirates</h6>
                          <span class="team-meta__place">Bebop Institute</span>
                        </div>
                      </div>
                    </td>
                    <td>45</td>
                    <td>5</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/sharks_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Sharks</h6>
                          <span class="team-meta__place">Marine College</span>
                        </div>
                      </div>
                    </td>
                    <td>42</td>
                    <td>8</td>
                    <td>3</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/alchemists_b_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">The Dota2Buff</h6>
                          <span class="team-meta__place">Eric Bros School</span>
                        </div>
                      </div>
                    </td>
                    <td>40</td>
                    <td>10</td>
                    <td>5</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/ocean_kings_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Ocean Kings</h6>
                          <span class="team-meta__place">Bay College</span>
                        </div>
                      </div>
                    </td>
                    <td>38</td>
                    <td>12</td>
                    <td>7</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/red_wings_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Red Wings</h6>
                          <span class="team-meta__place">Icarus College</span>
                        </div>
                      </div>
                    </td>
                    <td>37</td>
                    <td>13</td>
                    <td>8</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/lucky_clovers_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Lucky Clovers</h6>
                          <span class="team-meta__place">St. Patrick’s Institute</span>
                        </div>
                      </div>
                    </td>
                    <td>34</td>
                    <td>16</td>
                    <td>11</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/draconians_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Draconians</h6>
                          <span class="team-meta__place">Draconians</span>
                        </div>
                      </div>
                    </td>
                    <td>31</td>
                    <td>19</td>
                    <td>14</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="team-meta">
                        <figure class="team-meta__logo">
                          <img src="assets/images/samples/logos/bloody_wave_shield.html" alt="">
                        </figure>
                        <div class="team-meta__info">
                          <h6 class="team-meta__name">Bloody Wave</h6>
                          <span class="team-meta__place">Atlantic School</span>
                        </div>
                      </div>
                    </td>
                    <td>30</td>
                    <td>20</td>
                    <td>15</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </aside>
        <aside class="widget widget--sidebar card widget-popular-posts">
          <div class="widget__title card__header">
            <h4>Popular News</h4>
          </div>
          <div class="widget__content card__content">
            <ul class="posts posts--simple-list">
              <li class="posts__item posts__item--category-2">
                <figure class="posts__thumb">
                  <a href="#"><img src="assets/images/samples/post-img1-xs.html" alt=""></a>
                </figure>
                <div class="posts__inner">
                  <div class="posts__cat">
                    <span class="label posts__cat-label">Injuries</span>
                  </div>
                  <h6 class="posts__title"><a href="#">Mark Johnson has a Tibia Fracture and is gonna be out</a></h6>
                  <time datetime="2016-08-23" class="posts__date">August 23rd, 2016</time>
                </div>
              </li>
              <li class="posts__item posts__item--category-1">
                <figure class="posts__thumb">
                  <a href="#"><img src="assets/images/samples/post-img2-xs.html" alt=""></a>
                </figure>
                <div class="posts__inner">
                  <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                  </div>
                  <h6 class="posts__title"><a href="#">Jay Rorks is only 24 points away from breaking the record</a></h6>
                  <time datetime="2016-08-22" class="posts__date">August 22nd, 2016</time>
                </div>
              </li>
              <li class="posts__item posts__item--category-1">
                <figure class="posts__thumb">
                  <a href="#"><img src="assets/images/samples/post-img3-xs.html" alt=""></a>
                </figure>
                <div class="posts__inner">
                  <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                  </div>
                  <h6 class="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6>
                  <time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                </div>
              </li>
              <li class="posts__item posts__item--category-1">
                <figure class="posts__thumb">
                  <a href="#"><img src="assets/images/samples/post-img4-xs.html" alt=""></a>
                </figure>
                <div class="posts__inner">
                  <div class="posts__cat">
                    <span class="label posts__cat-label">The Team</span>
                  </div>
                  <h6 class="posts__title"><a href="#">The team is starting a new power breakfast regimen</a></h6>
                  <time datetime="2016-08-21" class="posts__date">August 21st, 2016</time>
                </div>
              </li>
            </ul>
          </div>
        </aside>
      </div>
    </div>

  </div>
</div>
{% endblock %}
