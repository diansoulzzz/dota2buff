<!DOCTYPE html>
<html>
<head>
  <title>{%block title %}Home {% endblock %} | Dota2Buff</title>
  <meta name="referrer" content="no-referrer">
  {% include "includes/styles.volt" %}
  {% block styles %}{% endblock %}
</head>
<body class="template-basketball">
  <div class="site-wrapper clearfix">
    <div class="site-overlay"></div>
    {% include "includes/header.volt" %}
    {{ this.flash.output() }}
    {# {% include "includes/sidebar.volt" %} #}
    {% block content %}{% endblock %}
    {% include "includes/footer.volt" %}
    {% include "includes/modal-login.volt" %}
  </div>
  {% include "includes/scripts.volt" %}
  {% block scripts %}{% endblock %}
  </body>
</html>
