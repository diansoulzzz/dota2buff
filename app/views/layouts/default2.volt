<!DOCTYPE html>
<html>
    <head>
        <title>{{ title }} - An example blog</title>
        {{ assets.outputCss("header")}}
        {{ assets.outputCss() }}
        {{stylesheet_link('assets/css/style.css')}}

    </head>
    <body>
        {% if show_navigation %}
            <ul id='navigation'>
                {% for item in menu %}
                    <li>
                        <a href='{{ item.href }}'>
                            {{ item.caption }}
                        </a>
                    </li>
                {% endfor %}
            </ul>
        {% endif %}

        <h1>{{ post.title }}</h1>

        <div class='content'>
            {% block content %}willoveride{% endblock %}
        </div>

    </body>
    {{javascript_include('assets/css/')}}
</html>
