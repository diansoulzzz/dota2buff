<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Alchemists - Sports News HTML Template">
<meta name="author" content="Dan Fisher">
<meta name="keywords" content="Sports News HTML Template">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

<link rel="shortcut icon" href="{{url('assets/images/favicons/favicon.ico')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{url('assets/images/favicons/favicon-120.css')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{url('assets/images/favicons/favicon-152.css')}}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CSource+Sans+Pro:400,700" rel="stylesheet">
<link href="{{url('assets/css/preloader.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{url('assets/fonts/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<link href="{{url('assets/vendor/slick/slick.css')}}" rel="stylesheet">
<link href="{{url('assets/css/style.css')}}" rel="stylesheet">
