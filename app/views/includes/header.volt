<div class="header-mobile clearfix" id="header-mobile">
  <div class="header-mobile__logo">
    <a href="{{url('')}}"><img src="{{url('assets/images/icon2.png')}}" srcset="{{url('assets/images/icon2.png')}} 2x" style="max-width: 80px;border:1" alt="Dota2Buff" class="header-mobile__logo-img"></a>
  </div>
  <div class="header-mobile__inner">
    <a id="header-mobile__toggle" class="burger-menu-icon"><span class="burger-menu-icon__line"></span></a>
    <span class="header-mobile__search-icon" id="header-mobile__search-icon"></span>
  </div>
</div>

<header class="header">
  <div class="header__top-bar clearfix">
    <div class="container">
      <ul class="nav-account">
        {% if session.has("auth") %}
            <li class="nav-account__item"><a href="{{url('players/'~session.get('auth')['accountid'])}}" >{{ session.get('auth')['personaname'] }}</a></li>
            <li class="nav-account__item nav-account__item--logout"><a href="{{url('logout')}}">Logout</a></li>
        {% else %}
            <li class="nav-account__item"><a href="{{ core.steam.loginurl }}" ><i class="fa fa-steam"></i> Sign In</a></li>
        {% endif  %}
      </ul>
    </div>
  </div>
  {# <div class="header__secondary">
    <div class="container">
      <div class="header-search-form">
        <form action="#" id="mobile-search-form" class="search-form">
          <input type="text" class="form-control header-mobile__search-control" value="" placeholder="Enter your seach here...">
          <button type="submit" class="header-mobile__search-submit"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <ul class="info-block info-block--header">
        <li class="info-block__item info-block__item--contact-primary">
          <svg role="img" class="df-icon df-icon--jersey">
            <use xlink:href="assets/images/icons-basket.html#jersey"/>
          </svg>
          <h6 class="info-block__heading">Join Our Team!</h6>
          <a class="info-block__link" href="mailto:tryouts@Dota2Buff.com">tryouts@Dota2Buff.com</a>
        </li>
        <li class="info-block__item info-block__item--contact-secondary">
          <svg role="img" class="df-icon df-icon--basketball">
            <use xlink:href="assets/images/icons-basket.html#basketball"/>
          </svg>
          <h6 class="info-block__heading">Contact Us</h6>
          <a class="info-block__link" href="mailto:info@Dota2Buff.com">info@Dota2Buff.com</a>
        </li>
        <li class="info-block__item info-block__item--shopping-cart">
          <a href="#" class="info-block__link-wrapper">
            <div class="df-icon-stack df-icon-stack--bag">
              <svg role="img" class="df-icon df-icon--bag">
                <use xlink:href="assets/images/icons-basket.html#bag"/>
              </svg>
              <svg role="img" class="df-icon df-icon--bag-handle">
                <use xlink:href="assets/images/icons-basket.html#bag-handle"/>
              </svg>
            </div>
            <h6 class="info-block__heading">Your Bag (8 items)</h6>
            <span class="info-block__cart-sum">$256,30</span>
          </a>
          <ul class="header-cart">
            <li class="header-cart__item">
              <figure class="header-cart__product-thumb">
                <a href="shop-product.html">
                  <img src="assets/images/samples/cart-sm-1.html" alt="">
                </a>
              </figure>
              <div class="header-cart__inner">
                <span class="header-cart__product-cat">Sneakers</span>
                <h5 class="header-cart__product-name"><a href="shop-product.html">Sundown Sneaker</a></h5>
                <div class="header-cart__product-ratings">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star empty"></i>
                </div>
                <div class="header-cart__product-sum">
                  <span class="header-cart__product-price">$28.00</span> x <span class="header-cart__product-count">2</span>
                </div>
                <div class="fa fa-times header-cart__close"></div>
              </div>
            </li>
            <li class="header-cart__item">
              <figure class="header-cart__product-thumb">
                <a href="shop-product.html">
                  <img src="assets/images/samples/cart-sm-2.html" alt="">
                </a>
              </figure>
              <div class="header-cart__inner">
                <span class="header-cart__product-cat">Sneakers</span>
                <h5 class="header-cart__product-name"><a href="shop-product.html">Atlantik Sneaker</a></h5>
                <div class="header-cart__product-ratings">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </div>
                <div class="header-cart__product-sum">
                  <span class="header-cart__product-price">$30.00</span> x <span class="header-cart__product-count">4</span>
                </div>
                <div class="fa fa-times header-cart__close"></div>
              </div>
            </li>
            <li class="header-cart__item">
              <figure class="header-cart__product-thumb">
                <a href="shop-product.html">
                  <img src="assets/images/samples/cart-sm-3.html" alt="">
                </a>
              </figure>
              <div class="header-cart__inner">
                <span class="header-cart__product-cat">Sneakers</span>
                <h5 class="header-cart__product-name"><a href="shop-product.html">Aquarium Sneaker</a></h5>
                <div class="header-cart__product-ratings">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star empty"></i>
                  <i class="fa fa-star empty"></i>
                </div>
                <div class="header-cart__product-sum">
                  <span class="header-cart__product-price">$26.00</span> x <span class="header-cart__product-count">1</span>
                </div>
                <div class="fa fa-times header-cart__close"></div>
              </div>
            </li>
            <li class="header-cart__item header-cart__item--subtotal">
              <span class="header-cart__subtotal">Cart Subtotal</span>
              <span class="header-cart__subtotal-sum">$282.00</span>
            </li>
            <li class="header-cart__item header-cart__item--action">
              <a href="shop-cart.html" class="btn btn-default btn-block">Go to Cart</a>
              <a href="shop-checkout.html" class="btn btn-primary-inverse btn-block">Checkout</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div> #}
  <div class="header__primary">
    <div class="container">
      <div class="header__primary-inner">
        <div class="header-logo">
          <a href="{{url('')}}"><img src="{{url('assets/images/icon2.png')}}" alt="Dota2Buff" srcset="{{url('assets/images/icon2.png')}} 2x" style="max-width: 75px;border:1" class="header-logo__img"></a>
        </div>
        <nav class="main-nav clearfix">
          <ul class="main-nav__list">
            <li class="{{ router.getRewriteUri()=='/' ? 'active':'' }}"><a href="{{url('')}}">Home</a></li>
            <li class="{{ router.getRewriteUri()=='/pro-players' ? 'active':'' }}"><a href="{{url('pro-players')}}">Pro Players</a></li>
            {# <li class="{{ stripos(router.getRewriteUri(),'/heroes') !== false ? 'active':'' }}"><a href="{{url('heroes')}}">Heroes</a></li> #}
            <li class="has-children {{ stripos(router.getRewriteUri(),'/heroes') !== false ? 'active':'' }}">
              <span class="main-nav__toggle"></span><a href="{{url('heroes')}}">Heroes</a>
              <ul class="main-nav__sub">
                <li><a href="{{url('heroes')}}">All Heroes</a></li>
                <li><a href="{{url('heroes/trends')}}">Trends</a></li>
              </ul>
            </li>
            <li class="{{ stripos(router.getRewriteUri(),'/items') !== false ? 'active':'' }}"><a href="{{url('items')}}">Items</a></li>
            <li class="{{ stripos(router.getRewriteUri(),'/story') !== false ? 'active':'' }}"><a href="{{url('story')}}">Story</a></li>
            <li class="{{ stripos(router.getRewriteUri(),'/event') !== false ? 'active':'' }}"><a href="{{url('event')}}">Event</a></li>
            <li class="{{ stripos(router.getRewriteUri(),'/schedule') !== false ? 'active':'' }}"><a href="{{url('schedule')}}">Schedule</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
