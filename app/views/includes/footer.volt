<footer id="footer" class="footer">
  <div class="footer-widgets">
    <div class="footer-widgets__inner">
      <div class="container">
        <div class="row">
          {# <div class="col-sm-12 col-md-3">
            <div class="footer-col-inner">
              <div class="footer-logo">
                <a href="{{url('')}}"><img src="{{url('assets/images/icon2.png')}}" srcset="{{url('assets/images/icon2.png')}} 2x" style="max-width: 80px;border:1" alt="Dota2Buff" class="footer-logo__img"></a>
              </div>
            </div>
          </div> #}
          {# <div class="col-sm-6 col-md-3">
            <div class="footer-col-inner">
              <div class="widget widget--footer widget-contact-info">
                <h4 class="widget__title">Contact Info</h4>
                <div class="widget__content">
                  <div class="widget-contact-info__desc">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                  </div>
                  <div class="widget-contact-info__body info-block">
                    <div class="info-block__item">
                      <svg role="img" class="df-icon df-icon--basketball">
                        <use xlink:href="assets/images/icons-basket.html#basketball"/>
                      </svg>
                      <h6 class="info-block__heading">Contact Us</h6>
                      <a class="info-block__link" href="mailto:info@dota2Buff.com">info@dota2Buff.com</a>
                    </div>
                    <div class="info-block__item">
                      <svg role="img" class="df-icon df-icon--jersey">
                        <use xlink:href="assets/images/icons-basket.html#jersey"/>
                      </svg>
                      <h6 class="info-block__heading">Join Our Team!</h6>
                      <a class="info-block__link" href="mailto:tryouts@dota2Buff.com">tryouts@dota2Buff.com</a>
                    </div>
                    <div class="info-block__item info-block__item--nopadding">
                      <ul class="social-links">
                        <li class="social-links__item">
                          <a href="#" class="social-links__link"><i class="fa fa-facebook"></i> Facebook</a>
                        </li>
                        <li class="social-links__item">
                          <a href="#" class="social-links__link"><i class="fa fa-twitter"></i> Twitter</a>
                        </li>
                        <li class="social-links__item">
                          <a href="#" class="social-links__link"><i class="fa fa-google-plus"></i> Google+</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> #}
        </div>
      </div>
    </div>
  </div>
  <div class="footer-secondary footer-secondary--has-decor">
    <div class="container">
      <div class="footer-secondary__inner">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <ul class="footer-nav">
              <li class="footer-nav__item"><a href="{{url('')}}">Home</a></li>
              <li class="footer-nav__item"><a href="{{url('pro-players')}}">Pro Players</a></li>
              <li class="footer-nav__item"><a href="{{url('heroes')}}">Heroes</a></li>
              <li class="footer-nav__item"><a href="{{url('items')}}">Items</a></li>
              <li class="footer-nav__item"><a href="{{url('story')}}">Story</a></li>
              <li class="footer-nav__item"><a href="{{url('event')}}">Event</a></li>
              <li class="footer-nav__item"><a href="{{url('schedule')}}">Schedule</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
