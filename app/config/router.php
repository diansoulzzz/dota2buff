<?php

$router = $di->getRouter();

// Define your routes here
// $router->add(
//     "/crawl",
//     [
//         "controller" => "crawl",
//         "action"     => "crawling",
//     ]
// );

// $router->add(
//     "/",
//     [
//         "controller" => "home",
//         "action"     => "index",
//     ]
// );
$router->add(
    "/matches/{matchid}",
    [
        "controller" => "matches",
        "action"     => "overview"
    ]
);
$router->add(
    "/players/{accountid}",
    [
        "controller" => "players",
        "action"     => "overview"
    ]
);
$router->add(
    "/players/{accountid}/matches",
    [
        "controller" => "players",
        "action"     => "matches"
    ]
);
$router->add(
    "/players/{accountid}/heroes",
    [
        "controller" => "players",
        "action"     => "heroes"
    ]
);
$router->add(
    "/players/{accountid}/histograms",
    [
        "controller" => "players",
        "action"     => "histograms"
    ]
);
$router->add(
    "/players/{accountid}/histograms/{type}",
    [
        "controller" => "players",
        "action"     => "histograms"
    ]
);
$router->add(
    "/heroes/{heroesid}",
    [
        "controller" => "heroes",
        "action"     => "overview"
    ]
);
$router->add(
    "/heroes/trends",
    [
        "controller" => "heroes",
        "action"     => "trends"
    ]
);
$router->add(
    "/items/{itemslink}",
    [
        "controller" => "items",
        "action"     => "overview"
    ]
);
$router->handle();
